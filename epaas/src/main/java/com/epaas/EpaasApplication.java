package com.epaas;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.ui.velocity.VelocityEngineFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.zaxxer.hikari.HikariDataSource;

import io.micrometer.core.instrument.MeterRegistry;

@SpringBootApplication
@EnableEurekaClient
@EnableJpaRepositories(basePackages = "com.epaas.*")
@EntityScan(basePackages = "com.epaas")
@EnableTransactionManagement
public class EpaasApplication implements CommandLineRunner {

	@Autowired
	private Environment environment;

	public static void main(String[] args) {
		SpringApplication.run(EpaasApplication.class, args);
	}

	@Configuration
	class RestTemplateConfig {
		@Bean
		@LoadBalanced
		public RestTemplate restTemplate() {
			return new RestTemplate();
		}
	}
	


	@Bean
	public VelocityEngine getVelocityEngine() throws VelocityException, IOException {
		VelocityEngineFactory factory = new VelocityEngineFactory();
		Properties props = new Properties();
		props.put("resource.loader", "class");
		props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		factory.setVelocityProperties(props);
		return factory.createVelocityEngine();
	}

	@Configuration
	class DatasourceMetricsConfig {

		@Autowired
		private DataSource dataSource;

		@Autowired
		private MeterRegistry metricRegistry;

		@PostConstruct
		public void setUpHikariWithMetrics() {
			if (dataSource instanceof HikariDataSource) {
				((HikariDataSource) dataSource).setMetricRegistry(metricRegistry);
			}
		}
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println(
				"=============================Starting REPORTING-SERVICE===========================================");
		System.out.println("CLIENT_PORT Value : " + environment.getProperty("CLIENT_PORT"));
		System.out.println("EUREKA_DEFAULT_ZONE Value : " + environment.getProperty("EUREKA_DEFAULT_ZONE"));
		System.out.println("DATASOURCE_URL Value : " + environment.getProperty("DATASOURCE_URL"));
		System.out.println("DATASOURCE_USERNAME Value : " + environment.getProperty("DATASOURCE_USERNAME"));
		System.out.println("DATASOURCE_PASSWORD Value : " + environment.getProperty("DATASOURCE_PASSWORD"));
		System.out.println("DATASOURCE_HIKARI_MAXIMUM_POOL_SIZE Value : "
				+ environment.getProperty("DATASOURCE_HIKARI_MAXIMUM_POOL_SIZE"));
		System.out.println("DATASOURCE_HIKARI_MINIMIUM_IDLE Value : "
				+ environment.getProperty("DATASOURCE_HIKARI_MINIMIUM_IDLE"));
		System.out.println(
				"DATASOURCE_HIKARI_IDLE_TIMEOUT Value : " + environment.getProperty("DATASOURCE_HIKARI_IDLE_TIMEOUT"));
		System.out.println(
				"DATASOURCE_HIKARI_AUTO_COMMIT Value : " + environment.getProperty("DATASOURCE_HIKARI_AUTO_COMMIT"));
		System.out
				.println("DATASOURCE_CONTINUEONERROR Value : " + environment.getProperty("DATASOURCE_CONTINUEONERROR"));
		System.out.println("JPA_GENERATE_DDL Value : " + environment.getProperty("JPA_GENERATE_DDL"));
		System.out.println("JPA_SHOW_SQL Value : " + environment.getProperty("JPA_SHOW_SQL"));
		System.out.println("JPA_HIBERNATE_DDL_AUTO Value : " + environment.getProperty("JPA_HIBERNATE_DDL_AUTO"));
		System.out.println("HIBERNATE_TEMP_USE_JDBC_METADATA_DEFAULTS Value : "
				+ environment.getProperty("HIBERNATE_TEMP_USE_JDBC_METADATA_DEFAULTS"));
		System.out.println("MULTIPART_ENABLED Value : " + environment.getProperty("MULTIPART_ENABLED"));
		System.out.println(
				"MULTIPART_FILE_SIZE_THRESHOLD Value : " + environment.getProperty("MULTIPART_FILE_SIZE_THRESHOLD"));
		System.out.println("MULTIPART_MAX_FILE_SIZE Value : " + environment.getProperty("MULTIPART_MAX_FILE_SIZE"));
		System.out
				.println("MULTIPART_MAX_REQUEST_SIZE Value : " + environment.getProperty("MULTIPART_MAX_REQUEST_SIZE"));
		System.out.println("PAGE_LIMIT Value : " + environment.getProperty("PAGE_LIMIT"));
		/*
		 * Added under the supervision of Hawa sir till the last changes on 01-05-2020
		 */
		System.out.println("EUREKA_HOSTNAME_NAME Value : " + environment.getProperty("EUREKA_HOSTNAME_NAME"));
		System.out.println("LEASE_RENEWAL_INTERVAl_IN_SECONDS Value : "
				+ environment.getProperty("LEASE_RENEWAL_INTERVAl_IN_SECONDS"));
		System.out.println("LEASE_EXPIRATION_DURATIONIN_SECONDS Value : "
				+ environment.getProperty("LEASE_EXPIRATION_DURATIONIN_SECONDS"));
		System.out.println("REGISTER_WITH_EUREKA Value : " + environment.getProperty("REGISTER_WITH_EUREKA"));
		System.out.println("FETCH_REGISTRY Value : " + environment.getProperty("FETCH_REGISTRY"));
		System.out.println("Email_Address Value : " + environment.getProperty("Email_Address"));

	}


@Bean

public CorsFilter corsFilter() {
      final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
      final CorsConfiguration config = new CorsConfiguration();
      config.setAllowCredentials(true);
      config.setAllowedOrigins(Collections.singletonList("*"));
      config.setAllowedHeaders(Collections.singletonList("*"));
      //config.setAllowedHeaders(Arrays.asList("Origin", "Content-Type", "Accept","Authorization"));
      config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"));
      source.registerCorsConfiguration("/**", config);
      return new CorsFilter(source);
  }
}