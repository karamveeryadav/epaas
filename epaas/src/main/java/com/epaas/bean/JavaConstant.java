package com.epaas.bean;

public class JavaConstant {
	
	public final static String QUERY_user_details="from MasterUser where user_Id=?1";
	public final static String QUERY_user_update=" update MasterUser SET user_pwd=?1, dev_pwd=?2, company_name=?3, name_of_business=?4, license_no=?5, email_id=?6, mobile_no=?7, user_img_path=?8 where user_id=?9 ";
	public final static String QUERY_16=" from MasterUser where email_id=?1";
	public final static String QUERY_ADDITIVE="select additiveName from MasterAdditive";
	public final static String QUERY_MasterApplicationType="select applicationName from MasterApplicationType";
	public final static String QUERY_agreement="select name from MasterCopyOfAgreementWithTheEntity";
	public final static String QUERY_food="select name from MasterFoodCategory";
	public final static String QUERY_ingredient="select ingredientsName from MasterIngredient";
	public final static String QUERY_module="select moduleName from MasterModule";
	public final static String QUERY_payment="select paymentModeName from MasterPaymentMode";
	public final static String QUERY_status="select regulatoryStatusName from MasterRegulatoryStatus";
	public final static String QUERY_role="select roleName from MasterRole";
	public final static String QUERY_safety="select safetyInformationName from MasterSafetyInformation";
	public final static String QUERY_source="select ingradientName from MasterSourceOfFoodIngradient";
	public final static String QUERY_STATUS="select statusId from MasterStatus";
	public final static String QUERY_submodule="select subModuleName from MasterSubModule";
	public final static String QUERY_type="select foodName from MasterTypeOfFood";
	public final static String QUERY_AA="select mapId from MasterUserVsSubModule";


	
}
