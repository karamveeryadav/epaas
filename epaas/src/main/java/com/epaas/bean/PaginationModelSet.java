package com.epaas.bean;

import java.util.HashSet;

public class PaginationModelSet {
		
	private int currentPageNo;
	private int totalPages;
	private int pageLimit;
	private Long totalRecords;
	private HashSet<?> paginationListRecords;
	
	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
	
	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public int getPageLimit() {
		return pageLimit;
	}

	public Long getTotalRecords() {
		return totalRecords;
	}

	public void setPageLimit(int pageLimit) {
		this.pageLimit = pageLimit;
	}

	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public HashSet<?> getPaginationListRecords() {
		return paginationListRecords;
	}

	public void setPaginationListRecords(HashSet<?> paginationListRecords) {
		this.paginationListRecords = paginationListRecords;
	}	
	
}
