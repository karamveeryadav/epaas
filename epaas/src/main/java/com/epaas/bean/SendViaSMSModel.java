package com.epaas.bean;

public class SendViaSMSModel {

	private String smsContent;

	private String toSms;
	
	private String templateId;
	
	public String getSmsContent() {
		return smsContent;
	}

	public void setSmsContent(String smsContent) {
		this.smsContent = smsContent;
	}

	public String getToSms() {
		return toSms;
	}

	public void setToSms(String toSms) {
		this.toSms = toSms;
	}

	public String getTemplateId() {
		return templateId;
	}

	public void setTemplateId(String templateId) {
		this.templateId = templateId;
	}

	
}
