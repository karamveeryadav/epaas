package com.epaas.bean;

public class Status {

	private String StatusCode;
	private String GeneratedCode;
	private String otp;
	private String registeredEmailId;
	private String mobileNumber;
	private String sentType;
	private String secondaryMobile;
	private String secondaryEmail;
	private String accessToken;

	public Status() {
	}

	public Status(String StatusCode, String GeneratedCode) {
		this.StatusCode = StatusCode;
		this.GeneratedCode = GeneratedCode;
	}
	
	public Status(String statusCode, String generatedCode, String otp) {
		StatusCode = statusCode;
		GeneratedCode = generatedCode;
		this.otp = otp;
	}

	public Status(String statusCode, String generatedCode, String sentType, String otp) {
		StatusCode = statusCode;
		GeneratedCode = generatedCode;
		this.sentType = sentType;
		this.otp=otp;
	}

	public String getSentType() {
		return sentType;
	}

	public void setSentType(String sentType) {
		this.sentType = sentType;
	}

	public Status(String statusCode, String generatedCode, String otp, String registeredEmailId, String mobileNumber) {
		StatusCode = statusCode;
		GeneratedCode = generatedCode;
		this.otp = otp;
		this.registeredEmailId = registeredEmailId;
		this.mobileNumber = mobileNumber;
	}
	
	
	public Status(String statusCode, String generatedCode, String otp, String registeredEmailId, String mobileNumber,String secondaryEmail,String secondaryMobile ) {
		StatusCode = statusCode;
		GeneratedCode = generatedCode;
		this.otp = otp;
		this.registeredEmailId = registeredEmailId;
		this.mobileNumber = mobileNumber;
		this.secondaryEmail =secondaryEmail;
		this.secondaryMobile= secondaryMobile;
	}
		
	public String getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(String StatusCode) {
		this.StatusCode = StatusCode;
	}

	public String getGeneratedCode() {
		return GeneratedCode;
	}

	public void setGeneratedCode(String GeneratedCode) {
		this.GeneratedCode = GeneratedCode;
	}

	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public String getRegisteredEmailId() {
		return registeredEmailId;
	}

	public void setRegisteredEmailId(String registeredEmailId) {
		this.registeredEmailId = registeredEmailId;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}


	public String getSecondaryEmail() {
		return secondaryEmail;
	}

	public void setSecondaryEmail(String secondaryEmail) {
		this.secondaryEmail = secondaryEmail;
	}

	public String getSecondaryMobile() {
		return secondaryMobile;
	}

	public void setSecondaryMobile(String secondaryMobile) {
		this.secondaryMobile = secondaryMobile;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}	
	
	
}

