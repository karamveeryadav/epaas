package com.epaas.bean;

import java.util.List;

import javax.persistence.ParameterMode;

public class StoredProcedureModel {
	private int currentPageNo;
	private int totalPages;
	private int pageLimit;
	private Long totalRecords;
	private String procedureName;
	private Object[] listOfParameters;
	private Object[] classtypeoflist;
	private ParameterMode[] parameterModes;
	private Exception exception;
	private List<?> paginationListRecords;

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}

	
	public int getCurrentPageNo() {
		return currentPageNo;
	}

	public void setCurrentPageNo(int currentPageNo) {
		this.currentPageNo = currentPageNo;
	}

	public int getPageLimit() {
		return pageLimit;
	}

	public Long getTotalRecords() {
		return totalRecords;
	}


	public void setPageLimit(int pageLimit) {
		this.pageLimit = pageLimit;
	}

	public void setTotalRecords(Long totalRecords) {
		this.totalRecords = totalRecords;
	}

	public List<?> getPaginationListRecords() {
		return paginationListRecords;
	}

	public void setPaginationListRecords(List<?> paginationListRecords) {
		this.paginationListRecords = paginationListRecords;
	}

	public String getProcedureName() {
		return procedureName;
	}

	public void setProcedureName(String procedureName) {
		this.procedureName = procedureName;
	}

	public Object[] getListOfParameters() {
		return listOfParameters;
	}

	public void setListOfParameters(Object[] listOfParameters) {
		this.listOfParameters = listOfParameters;
	}

	public Object[] getClasstypeoflist() {
		return classtypeoflist;
	}

	public void setClasstypeoflist(Object[] classtypeoflist) {
		this.classtypeoflist = classtypeoflist;
	}

	public ParameterMode[] getParameterModes() {
		return parameterModes;
	}

	public void setParameterModes(ParameterMode[] parameterModes) {
		this.parameterModes = parameterModes;
	}



	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}
	

}
