package com.epaas.contoller;

import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.epaas.bean.OtpMessageSender;
import com.epaas.bean.Status;
import com.epaas.services.CommonService;

@RestController
@RequestMapping(value = "/")
public class CommonController {

	@Autowired
	CommonService commonService;

	@RequestMapping(value = "sendOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status sendOTP(@RequestBody OtpMessageSender otpMessageSender) {
		try {
			Status status = commonService.sendOTP(otpMessageSender);
			if (status.getStatusCode().equals("200M"))
				return status;
			if (status.getStatusCode().equals("200E"))
				return status;
			if (status.getStatusCode().equals("203M"))
				return status;
			else
				return status;
		} catch (Exception e) {
			e.printStackTrace();
			return new Status(String.valueOf(HttpServletResponse.SC_EXPECTATION_FAILED), e.toString());
		}
	}

//----------Forgot Password------

	@RequestMapping(value = "sendForgotPwdOTP", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Status sendOTPForForgotPwd(@RequestBody OtpMessageSender otpMessageSender) {

		try {
			Status status = null;
			status=commonService.sendOtpForForgotPwd(otpMessageSender);
			if (status.getStatusCode().equals("200M"))
				return status;
			if (status.getStatusCode().equals("200E"))
				return status;
			if (status.getStatusCode().equals("203M"))
				return status;
			else
				return status;
		} catch (Exception e) {
			e.printStackTrace();
			return new Status(String.valueOf(HttpServletResponse.SC_EXPECTATION_FAILED), e.toString());
		}
	}
	
//-------------GENERATE CAPTCHA CODE--------
	
	@RequestMapping(value = "captcha", method = RequestMethod.GET)
	public @ResponseBody void captchaRequest(HttpServletResponse response)
			throws Exception {
		try {
			commonService.captchaRequest(response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
