package com.epaas.contoller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.epaas.services.ReportingServices;

@RestController
@RequestMapping("/")
public class ReportingController {
	
	@Autowired
	public ReportingServices reportingServices;
	
	@RequestMapping(value = "getMasterAdditive", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterAdditive(){
		List<?> additiveList=null;
		try {
			additiveList = reportingServices.getMasterAdditive();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return additiveList;
	}
	
	@RequestMapping(value = "getMasterApplicationType", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterApplicationType(){
		List<?> applicationList=null;
		try {
			applicationList = reportingServices.getMasterApplicationType();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return applicationList;
	}
	
	@RequestMapping(value = "getMasterCopyOfAgreementwithTheEntity", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterCopyOfAgreementwithTheEntity(){
		List<?> agreementList=null;
		try {
			agreementList = reportingServices.getMasterCopyOfAgreementwithTheEntity();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return agreementList;
	}
	
	@RequestMapping(value = "getMasterFoodCategory", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterFoodCategory(){
		List<?> foodList=null;
		try {
			foodList = reportingServices.getMasterFoodCategory();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return foodList;
	}
	
	@RequestMapping(value = "getMasterIngredient", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterIngredient(){
		List<?> ingredientList=null;
		try {
			ingredientList = reportingServices.getMasterIngredient();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ingredientList;
	}
	
	@RequestMapping(value = "getMasterModule", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterModule(){
		List<?> moduleList=null;
		try {
			moduleList = reportingServices.getMasterModule();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return moduleList;
	}
	
	@RequestMapping(value = "getMasterPaymentMode", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterPaymentMode(){
		List<?> paymentList=null;
		try {
			paymentList = reportingServices.getMasterPaymentMode();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return paymentList;
	}
	
	@RequestMapping(value = "getMasterRegulatoryStatus", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterRegulatoryStatus(){
		List<?> statusList=null;
		try {
			statusList = reportingServices.getMasterRegulatoryStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statusList;
	}
	
	@RequestMapping(value = "getMasterRole", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterRole(){
		List<?> roleList=null;
		try {
			roleList = reportingServices.getMasterRole();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return roleList;
	}
	
	@RequestMapping(value = "getMasterSafetyInformation", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterSafetyInformation(){
		List<?> safetyList=null;
		try {
			safetyList = reportingServices.getMasterSafetyInformation();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return safetyList;
	}
	
	@RequestMapping(value = "getMasterSourceOfFoodIngradient", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterSourceOfFoodIngradient(){
		List<?> list=null;
		try {
			list = reportingServices.getMasterSourceOfFoodIngradient();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	@RequestMapping(value = "getMasterStatus", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterStatus(){
		List<?> statuslist=null;
		try {
			statuslist = reportingServices.getMasterStatus();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return statuslist;
	}
	
	@RequestMapping(value = "getMasterSubModule", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterSubModule(){
		List<?>moduleList=null;
		try {
			moduleList = reportingServices.getMasterSubModule();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return moduleList;
	}
	
	@RequestMapping(value = "getMasterTypeOfFood", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterTypeOfFood(){
		List<?>typeList=null;
		try {
			typeList = reportingServices.getMasterTypeOfFood();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return typeList;
	}
	
	@RequestMapping(value = "getMasterUserVsSubModule", method = RequestMethod.GET)
	public @ResponseBody List<?> getMasterUserVsSubModule(){
		List<?>vsList=null;
		try {
			vsList = reportingServices.getMasterUserVsSubModule();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return vsList;
	}
}
