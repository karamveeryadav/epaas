package com.epaas.contoller;

import java.math.BigInteger;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.epaas.bean.Status;
import com.epaas.dto.MasterUserDto;
import com.epaas.services.SignupServices;

@RestController
@RequestMapping("/")
public class SignUpController {

	@Autowired
	SignupServices signupServices;

	@PostMapping(value = "saveuserdetails")
	public @ResponseBody Status saveUserDetails(@RequestBody MasterUserDto masterUserDto) throws Exception {
		try {
			String status = signupServices.saveUserDetails(masterUserDto);
			if (status.equals("Saved")) {
				return new Status(String.valueOf(HttpServletResponse.SC_OK), masterUserDto.getUserId() + "");
			} else {
				return new Status(String.valueOf(HttpServletResponse.SC_BAD_REQUEST),
						String.valueOf(HttpServletResponse.SC_OK));
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new Status(String.valueOf(HttpServletResponse.SC_EXPECTATION_FAILED), e.toString());
		}
	}

	@GetMapping(value = "viewsuserdetails/{userId}")
	public @ResponseBody Object getUserDetails(@PathVariable("userId") BigInteger userId) {
		return signupServices.getUserDetails(userId);
	}

	@PostMapping(value = "updateuserdetails")
	public @ResponseBody Status updateUserDetails(@RequestBody  MasterUserDto masterUserDto) {
		try {
			if (signupServices.updateUserDetails(masterUserDto))
				return new Status(String.valueOf(HttpServletResponse.SC_OK), masterUserDto.getUserId() + "");
			return new Status(String.valueOf(HttpServletResponse.SC_OK), "");
		} catch (Exception e) {
			e.printStackTrace();
			return new Status(String.valueOf(HttpServletResponse.SC_EXPECTATION_FAILED), "");
		}
	}

}