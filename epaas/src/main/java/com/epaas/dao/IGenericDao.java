package com.epaas.dao;

import java.util.List;

import com.epaas.bean.PaginationModel;
import com.epaas.bean.StoredProcedureModel;


public interface IGenericDao {
	
	   public <T> void save(final T entity);
	   public <T> void update(final T entity);
	   public <T> void delete(final T entity);
	   public <T> List<T> executeDDLHQL(String hqlquery, Object[] listofparameter);
	   public void executeDMLHQL(String hqlquery, Object[] listofparameter);
	   public <T> List<T> executeDDLSQL(String sqlquery, Object[] listofparameter);
	   PaginationModel getPaginationWithQuery(PaginationModel paginationModel, Integer currentPage, String hqlquery, Object[] listofparameter);
	   PaginationModel getPaginationWithSQLQuery(PaginationModel paginationModel, Integer currentPage, String sqlquery, Object[] listofparameter);
	   public StoredProcedureModel executeStoreProcedure(StoredProcedureModel storedProcedureModel,Integer currentPage );
	   public StoredProcedureModel getPaginationWithSQLQuery(StoredProcedureModel paginationModel, Integer currentPage);
	   PaginationModel getPaginationWithSQLQueryWithoutPagesCount(PaginationModel paginationModel, Integer currentPage, String hqlquery, Object[] listofparameter);
	   PaginationModel getPaginationWithQueryWithoutPagesCount(PaginationModel paginationModel, Integer currentPage, String hqlquery, Object[] listofparameter);

}
