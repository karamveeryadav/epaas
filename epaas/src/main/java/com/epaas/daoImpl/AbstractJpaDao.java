package com.epaas.daoImpl;

import java.io.IOException;
import java.math.BigInteger;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

import org.hibernate.transform.AliasToEntityMapResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.env.Environment;

import com.epaas.CountQueryEvent;
import com.epaas.CountQueryResultHolder;
import com.epaas.bean.PaginationModel;
import com.epaas.bean.StoredProcedureModel;

public abstract class AbstractJpaDao {
	 
	   @PersistenceContext
	   EntityManager entityManager;
	   
	   @Autowired
	   Environment environment;
	   
	   @Autowired
	   CountQueryResultHolder countQueryResultHolder;
	   
	   @Autowired
	   ApplicationEventPublisher applicationEventPublisher;
	   
	   public <T> void save( T entity ){
		      entityManager.persist( entity );
		   }
	   
	   public <T> void update( T entity ){
	      entityManager.merge( entity );
	   }
	 
	   public <T> void delete( T entity ){
	      entityManager.remove( entity );
	   }
	   @SuppressWarnings("unchecked")
	   public <T> List< T > executeDDLHQL(String hqlquery, Object[] listofparameter){
		   List< T > list =null;
		   try {
			   Query query =entityManager.createQuery( hqlquery );
			   for (int c = 0; c < listofparameter.length; c++) {
					query.setParameter(c+1, listofparameter[c]);
				}
			   list=query.getResultList();
		  }catch(Exception e) {
			  e.printStackTrace();
		  }
		  return list;
	   }
	   public void executeDMLHQL(String hqlquery, Object[] listofparameter){
		   try {
			   Query query =entityManager.createQuery( hqlquery );
			   for (int c = 0; c < listofparameter.length; c++) {
					query.setParameter(c+1, listofparameter[c]);
				}
			   query.executeUpdate();
		  }catch(Exception e) {
			  e.printStackTrace();
		  }
	   }
   
	   /***** Pagination for Query *******/
	   	public PaginationModel getPaginationWithQuery(PaginationModel paginationModel, Integer currentPage, String hqlquery, Object[] listofparameter){
	   		try {
				Integer pageLimit = getPageLimit();
				Query query = entityManager.createQuery(hqlquery);
				for (int c = 0; c < listofparameter.length; c++) {
					query.setParameter(c + 1, listofparameter[c]);
				}
				UUID identifier = UUID.randomUUID();
				CountQueryEvent countQueryEvent = new CountQueryEvent(this, hqlquery, listofparameter, identifier);
				applicationEventPublisher.publishEvent(countQueryEvent);
				while(!countQueryResultHolder.hasQueryTask(identifier)) {}
				CompletableFuture<Long> result = countQueryResultHolder.getQueryResult(identifier);
				Long totalRecords = result.get();
				System.out.println("TotalRecords: " + totalRecords);
				Integer totalPages = ((int) (Math.ceil((totalRecords+pageLimit-1)/pageLimit)));
				paginationModel.setCurrentPageNo(currentPage);
				paginationModel.setTotalRecords(totalRecords);
				query.setFirstResult((currentPage * pageLimit) - pageLimit);
				query.setMaxResults(pageLimit);
				paginationModel.setTotalPages(totalPages);
				paginationModel.setPageLimit(pageLimit);
				paginationModel.setPaginationListRecords( query.getResultList());
				countQueryResultHolder.removeCountQuery(identifier.toString());
				countQueryResultHolder.removeQueryResult(identifier);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return paginationModel;
		}
	   	
	   	
	   	/***** Pagination for SQL Query *******/
	   	@SuppressWarnings("deprecation")
		public PaginationModel getPaginationWithSQLQuery(PaginationModel paginationModel, Integer currentPage, String sqlquery, Object[] listofparameter){
			try {
				Integer pageLimit = getPageLimit();
				Query query = entityManager.createNativeQuery(sqlquery);
				query.unwrap( org.hibernate.query.Query.class ).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
				for (int c = 0; c < listofparameter.length; c++) {
					query.setParameter(c + 1, listofparameter[c]);
				}
				Query countQuery = entityManager.createNativeQuery("select count(*) from ( " + sqlquery + " ) x");
				for (int c = 0; c < listofparameter.length; c++) {
					countQuery.setParameter(c + 1, listofparameter[c]);
				}
				Long totalRecords = ((BigInteger) countQuery.getSingleResult()).longValue();
				Integer totalPages = ((int) (Math.ceil((totalRecords+pageLimit-1)/pageLimit)));
				paginationModel.setCurrentPageNo(currentPage);
				paginationModel.setTotalRecords(Long.valueOf(totalRecords));
				query.setFirstResult((currentPage * pageLimit) - pageLimit);
				query.setMaxResults(pageLimit);
				paginationModel.setTotalPages(totalPages);
				paginationModel.setPageLimit(pageLimit);
				paginationModel.setPaginationListRecords( query.getResultList());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return paginationModel;
		}
	   
		public int getPageLimit() throws IOException {
			if(environment.getProperty("PAGE_LIMIT") != null)
				return Integer.parseInt(environment.getProperty("PAGE_LIMIT"));
			else
				return 10;
		}
	   @SuppressWarnings({ "unchecked", "deprecation" })
	   public <T> List< T > executeDDLSQL(String sqlquery, Object[] listofparameter){
		   List< T > list =null;
		   try {
			   Query query =entityManager.createNativeQuery(sqlquery);
			   query.unwrap( org.hibernate.query.Query.class ).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
			   for (int c = 0; c < listofparameter.length; c++) {
					query.setParameter(c+1, listofparameter[c]);
				}
			   list=query.getResultList();
		  }catch(Exception e) {
			  e.printStackTrace();
		  }
		  return list;
	   }
	   
	   @SuppressWarnings("rawtypes")
	public StoredProcedureModel executeStoreProcedure(StoredProcedureModel storedProcedureModel,Integer currentPage ){
		   
		   try {
			   StoredProcedureQuery procedureQuery = entityManager.createStoredProcedureQuery(storedProcedureModel.getProcedureName());
			   	Class[] classtypeoflist = {Integer.class,String.class};
			   	ParameterMode[] parameterModes = {ParameterMode.IN,ParameterMode.IN};
			   	// ParameterMode[] parameterModes = storedProcedureModel.getParameterModes();
			   	Object[] listOfParameters = storedProcedureModel.getListOfParameters();
			   	for (int i = 0; i < storedProcedureModel.getListOfParameters().length; i++) {
			   	procedureQuery.registerStoredProcedureParameter(i+1,classtypeoflist[i],parameterModes[i]);
			   	procedureQuery.setParameter(i+1, listOfParameters[i]);
			   	}
			   	if(procedureQuery.execute()) {		   	
				   	Integer pageLimit = getPageLimit();
					Integer totalRecords = procedureQuery.getResultList().size();
					Integer totalPages = ((int) (Math.ceil((totalRecords+pageLimit-1)/pageLimit)));
					storedProcedureModel.setCurrentPageNo(currentPage);
					storedProcedureModel.setTotalRecords(Long.valueOf(totalRecords));
					procedureQuery.setFirstResult((currentPage * pageLimit) - pageLimit);
					procedureQuery.setMaxResults(pageLimit);
					storedProcedureModel.setTotalPages(totalPages);
					storedProcedureModel.setPageLimit(pageLimit);
					storedProcedureModel.setPaginationListRecords( procedureQuery.getResultList());
			   	}
			   }catch(Exception e) {
				   storedProcedureModel.setException(e);
				   e.printStackTrace();
			   }
		   		return storedProcedureModel;
		   	}
	   
	   /***** Pagination for SQL Query *******/
	   	@SuppressWarnings("deprecation")
		public StoredProcedureModel getPaginationWithSQLQuery(StoredProcedureModel storedProcedureModel, Integer currentPage){
			try {
				 Object[] listofparameter = storedProcedureModel.getListOfParameters();
				Integer pageLimit = getPageLimit();
				StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery(storedProcedureModel.getProcedureName());
				query.unwrap( org.hibernate.query.Query.class ).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
				for (int c = 0; c < storedProcedureModel.getListOfParameters().length; c++) {
					query.setParameter(c, listofparameter[c]);
				}
				Integer totalRecords = query.getResultList().size();
				Integer totalPages = ((int) (Math.ceil((totalRecords+pageLimit-1)/pageLimit)));
				storedProcedureModel.setCurrentPageNo(currentPage);
				storedProcedureModel.setTotalRecords(Long.valueOf(totalRecords));
				query.setFirstResult((currentPage * pageLimit) - pageLimit);
				query.setMaxResults(pageLimit);
				storedProcedureModel.setTotalPages(totalPages);
				storedProcedureModel.setPageLimit(pageLimit);
				storedProcedureModel.setPaginationListRecords( query.getResultList());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return storedProcedureModel;
		}
	   	
	 	/***** Pagination for Query *******/
        public PaginationModel getPaginationWithQueryWithoutPagesCount(PaginationModel paginationModel, Integer currentPage, String hqlquery, Object[] listofparameter){
                try {
                     Integer pageLimit = getPageLimit();
                     Query query = entityManager.createQuery(hqlquery);
                     for (int c = 0; c < listofparameter.length; c++) {
                             query.setParameter(c + 1, listofparameter[c]);
                     }
                     paginationModel.setCurrentPageNo(currentPage);
                     paginationModel.setTotalRecords(0L);
                     query.setFirstResult((currentPage * pageLimit) - pageLimit);
                     query.setMaxResults(pageLimit);
                     paginationModel.setTotalPages(0);
                     paginationModel.setPageLimit(pageLimit);
                     paginationModel.setPaginationListRecords( query.getResultList());
             } catch (Exception e) {
                     e.printStackTrace();
             }
             return paginationModel;
     }
        
     /***** Pagination for SQL Query *******/
     @SuppressWarnings("deprecation")
     public PaginationModel getPaginationWithSQLQueryWithoutPagesCount(PaginationModel paginationModel, Integer currentPage, String sqlquery, Object[] listofparameter){
             try {
                     Integer pageLimit = getPageLimit();
                     Query query = entityManager.createNativeQuery(sqlquery);
                     query.unwrap( org.hibernate.query.Query.class ).setResultTransformer(AliasToEntityMapResultTransformer.INSTANCE);
                     for (int c = 0; c < listofparameter.length; c++) {
                             query.setParameter(c + 1, listofparameter[c]);
                     }
                     paginationModel.setCurrentPageNo(currentPage);
                     paginationModel.setTotalRecords(0L);
                     query.setFirstResult((currentPage * pageLimit) - pageLimit);
                     query.setMaxResults(pageLimit);
                     paginationModel.setTotalPages(0);
                     paginationModel.setPageLimit(pageLimit);
                     paginationModel.setPaginationListRecords( query.getResultList());
             } catch (Exception e) {
                     e.printStackTrace();
             }
             return paginationModel;
     }
	   	
	}
