package com.epaas.dto;

import java.math.BigInteger;
import java.sql.Timestamp;

public class MasterUserDto {

	private static final long serialVersionUID = 1L;
	private int userId;
	private int loginId;
	private String userPwd;
	private String devPwd;
	private String companyName;
	private String nameOfBusiness;
	private BigInteger licenseNo;
	private String emailId;
	private boolean emailIdVerifiedFlag; 
	private BigInteger mobileNo;
	private boolean mobileNoVerifiedFlag;
	private String userImgPath;
	private boolean aciveFlag;
	private int roleId;
	private Timestamp lastLoginDateAndTime;
	private Timestamp lastChangePasswordDateAndTime;
	private BigInteger createdBy; 
	private Timestamp createdOn;
	private BigInteger updatedBy;
	private Timestamp updatedOn;
	

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getLoginId() {
		return loginId;
	}

	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getDevPwd() {
		return devPwd;
	}

	public void setDevPwd(String devPwd) {
		this.devPwd = devPwd;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getNameOfBusiness() {
		return nameOfBusiness;
	}

	public void setNameOfBusiness(String nameOfBusiness) {
		this.nameOfBusiness = nameOfBusiness;
	}

	public BigInteger getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(BigInteger licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public boolean isEmailIdVerifiedFlag() {
		return emailIdVerifiedFlag;
	}

	public void setEmailIdVerifiedFlag(boolean emailIdVerifiedFlag) {
		this.emailIdVerifiedFlag = emailIdVerifiedFlag;
	}

	public BigInteger getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(BigInteger mobileNo) {
		this.mobileNo = mobileNo;
	}

	public boolean isMobileNoVerifiedFlag() {
		return mobileNoVerifiedFlag;
	}

	public void setMobileNoVerifiedFlag(boolean mobileNoVerifiedFlag) {
		this.mobileNoVerifiedFlag = mobileNoVerifiedFlag;
	}

	public String getUserImgPath() {
		return userImgPath;
	}

	public void setUserImgPath(String userImgPath) {
		this.userImgPath = userImgPath;
	}

	public boolean isAciveFlag() {
		return aciveFlag;
	}

	public void setAciveFlag(boolean aciveFlag) {
		this.aciveFlag = aciveFlag;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Timestamp getLastLoginDateAndTime() {
		return lastLoginDateAndTime;
	}

	public void setLastLoginDateAndTime(Timestamp lastLoginDateAndTime) {
		this.lastLoginDateAndTime = lastLoginDateAndTime;
	}

	public Timestamp getLastChangePasswordDateAndTime() {
		return lastChangePasswordDateAndTime;
	}

	public void setLastChangePasswordDateAndTime(Timestamp lastChangePasswordDateAndTime) {
		this.lastChangePasswordDateAndTime = lastChangePasswordDateAndTime;
	}

	public BigInteger getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigInteger createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public BigInteger getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigInteger updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}



}
