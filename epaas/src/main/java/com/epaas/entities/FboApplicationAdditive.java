package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the fbo_application_additive database table.
 * 
 */
@Entity
@Table(name="fbo_application_additive")
@NamedQuery(name="FboApplicationAdditive.findAll", query="SELECT f FROM FboApplicationAdditive f")
public class FboApplicationAdditive implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="fbo_application_additive_id", unique=true, nullable=false)
	private Integer fboApplicationAdditiveId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="fbo_application_additive_name", length=100)
	private String fboApplicationAdditiveName;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to FboApplicationDetail
	@ManyToOne
	@JoinColumn(name="ref_id", nullable=false)
	private FboApplicationDetail fboApplicationDetail;

	public FboApplicationAdditive() {
	}

	public Integer getFboApplicationAdditiveId() {
		return this.fboApplicationAdditiveId;
	}

	public void setFboApplicationAdditiveId(Integer fboApplicationAdditiveId) {
		this.fboApplicationAdditiveId = fboApplicationAdditiveId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getFboApplicationAdditiveName() {
		return this.fboApplicationAdditiveName;
	}

	public void setFboApplicationAdditiveName(String fboApplicationAdditiveName) {
		this.fboApplicationAdditiveName = fboApplicationAdditiveName;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public FboApplicationDetail getFboApplicationDetail() {
		return this.fboApplicationDetail;
	}

	public void setFboApplicationDetail(FboApplicationDetail fboApplicationDetail) {
		this.fboApplicationDetail = fboApplicationDetail;
	}

}