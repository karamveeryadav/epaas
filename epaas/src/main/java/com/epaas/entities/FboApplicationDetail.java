package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the fbo_application_details database table.
 * 
 */
@Entity
@Table(name="fbo_application_details")
@NamedQuery(name="FboApplicationDetail.findAll", query="SELECT f FROM FboApplicationDetail f")
public class FboApplicationDetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ref_id", unique=true, nullable=false)
	private Integer refId;

	@Column(name="applicant_id", nullable=false)
	private Integer applicantId;

	@Column(name="applicant_name", length=100)
	private String applicantName;

	@Column(name="certificate_of_analysis_third_party_img", length=500)
	private String certificateOfAnalysisThirdPartyImg;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="email_id", nullable=false, length=100)
	private String emailId;

	@Column(name="functional_use", length=100)
	private String functionalUse;

	@Column(name="intended_use", length=100)
	private String intendedUse;

	@Column(name="justification_for_name", length=100)
	private String justificationForName;

	@Column(name="license_no")
	private Long licenseNo;

	@Column(name="manufacturing_process_img", length=500)
	private String manufacturingProcessImg;

	@Column(name="mobile_no", nullable=false)
	private Integer mobileNo;

	@Column(name="name_of_authorized_person", length=100)
	private String nameOfAuthorizedPerson;

	@Column(name="name_of_food", length=100)
	private String nameOfFood;

	@Column(name="nature_of_business", length=100)
	private String natureOfBusiness;

	@Column(name="organisation_address", length=500)
	private String organisationAddress;

	@Column(name="organisation_name", length=500)
	private String organisationName;

	@Column(name="proposed_product_category", length=100)
	private String proposedProductCategory;

	@Column(name="source_of_food_ingradient", length=200)
	private String sourceOfFoodIngradient;

	@Column(name="type_of_food", length=100)
	private String typeOfFood;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to FboApplicationAdditive
	@OneToMany(mappedBy="fboApplicationDetail")
	private List<FboApplicationAdditive> fboApplicationAdditives;

	//bi-directional many-to-one association to FboApplicationIngradient
	@OneToMany(mappedBy="fboApplicationDetail")
	private List<FboApplicationIngradient> fboApplicationIngradients;

	//bi-directional one-to-one association to FboApplicationPayment
	@OneToOne(mappedBy="fboApplicationDetail")
	private FboApplicationPayment fboApplicationPayment;

	//bi-directional many-to-one association to FboApplicationPaymentLog
	@OneToMany(mappedBy="fboApplicationDetail")
	private List<FboApplicationPaymentLog> fboApplicationPaymentLogs;

	//bi-directional many-to-one association to FboApplicationProcessLog
	@OneToMany(mappedBy="fboApplicationDetail")
	private List<FboApplicationProcessLog> fboApplicationProcessLogs;

	//bi-directional many-to-one association to MasterPaymentMode
	@OneToMany(mappedBy="fboApplicationDetail")
	private List<MasterPaymentMode> masterPaymentModes;

	public FboApplicationDetail() {
	}

	public Integer getRefId() {
		return this.refId;
	}

	public void setRefId(Integer refId) {
		this.refId = refId;
	}

	public Integer getApplicantId() {
		return this.applicantId;
	}

	public void setApplicantId(Integer applicantId) {
		this.applicantId = applicantId;
	}

	public String getApplicantName() {
		return this.applicantName;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public String getCertificateOfAnalysisThirdPartyImg() {
		return this.certificateOfAnalysisThirdPartyImg;
	}

	public void setCertificateOfAnalysisThirdPartyImg(String certificateOfAnalysisThirdPartyImg) {
		this.certificateOfAnalysisThirdPartyImg = certificateOfAnalysisThirdPartyImg;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getEmailId() {
		return this.emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getFunctionalUse() {
		return this.functionalUse;
	}

	public void setFunctionalUse(String functionalUse) {
		this.functionalUse = functionalUse;
	}

	public String getIntendedUse() {
		return this.intendedUse;
	}

	public void setIntendedUse(String intendedUse) {
		this.intendedUse = intendedUse;
	}

	public String getJustificationForName() {
		return this.justificationForName;
	}

	public void setJustificationForName(String justificationForName) {
		this.justificationForName = justificationForName;
	}

	public Long getLicenseNo() {
		return this.licenseNo;
	}

	public void setLicenseNo(Long licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getManufacturingProcessImg() {
		return this.manufacturingProcessImg;
	}

	public void setManufacturingProcessImg(String manufacturingProcessImg) {
		this.manufacturingProcessImg = manufacturingProcessImg;
	}

	public Integer getMobileNo() {
		return this.mobileNo;
	}

	public void setMobileNo(Integer mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getNameOfAuthorizedPerson() {
		return this.nameOfAuthorizedPerson;
	}

	public void setNameOfAuthorizedPerson(String nameOfAuthorizedPerson) {
		this.nameOfAuthorizedPerson = nameOfAuthorizedPerson;
	}

	public String getNameOfFood() {
		return this.nameOfFood;
	}

	public void setNameOfFood(String nameOfFood) {
		this.nameOfFood = nameOfFood;
	}

	public String getNatureOfBusiness() {
		return this.natureOfBusiness;
	}

	public void setNatureOfBusiness(String natureOfBusiness) {
		this.natureOfBusiness = natureOfBusiness;
	}

	public String getOrganisationAddress() {
		return this.organisationAddress;
	}

	public void setOrganisationAddress(String organisationAddress) {
		this.organisationAddress = organisationAddress;
	}

	public String getOrganisationName() {
		return this.organisationName;
	}

	public void setOrganisationName(String organisationName) {
		this.organisationName = organisationName;
	}

	public String getProposedProductCategory() {
		return this.proposedProductCategory;
	}

	public void setProposedProductCategory(String proposedProductCategory) {
		this.proposedProductCategory = proposedProductCategory;
	}

	public String getSourceOfFoodIngradient() {
		return this.sourceOfFoodIngradient;
	}

	public void setSourceOfFoodIngradient(String sourceOfFoodIngradient) {
		this.sourceOfFoodIngradient = sourceOfFoodIngradient;
	}

	public String getTypeOfFood() {
		return this.typeOfFood;
	}

	public void setTypeOfFood(String typeOfFood) {
		this.typeOfFood = typeOfFood;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public List<FboApplicationAdditive> getFboApplicationAdditives() {
		return this.fboApplicationAdditives;
	}

	public void setFboApplicationAdditives(List<FboApplicationAdditive> fboApplicationAdditives) {
		this.fboApplicationAdditives = fboApplicationAdditives;
	}

	public FboApplicationAdditive addFboApplicationAdditive(FboApplicationAdditive fboApplicationAdditive) {
		getFboApplicationAdditives().add(fboApplicationAdditive);
		fboApplicationAdditive.setFboApplicationDetail(this);

		return fboApplicationAdditive;
	}

	public FboApplicationAdditive removeFboApplicationAdditive(FboApplicationAdditive fboApplicationAdditive) {
		getFboApplicationAdditives().remove(fboApplicationAdditive);
		fboApplicationAdditive.setFboApplicationDetail(null);

		return fboApplicationAdditive;
	}

	public List<FboApplicationIngradient> getFboApplicationIngradients() {
		return this.fboApplicationIngradients;
	}

	public void setFboApplicationIngradients(List<FboApplicationIngradient> fboApplicationIngradients) {
		this.fboApplicationIngradients = fboApplicationIngradients;
	}

	public FboApplicationIngradient addFboApplicationIngradient(FboApplicationIngradient fboApplicationIngradient) {
		getFboApplicationIngradients().add(fboApplicationIngradient);
		fboApplicationIngradient.setFboApplicationDetail(this);

		return fboApplicationIngradient;
	}

	public FboApplicationIngradient removeFboApplicationIngradient(FboApplicationIngradient fboApplicationIngradient) {
		getFboApplicationIngradients().remove(fboApplicationIngradient);
		fboApplicationIngradient.setFboApplicationDetail(null);

		return fboApplicationIngradient;
	}

	public FboApplicationPayment getFboApplicationPayment() {
		return this.fboApplicationPayment;
	}

	public void setFboApplicationPayment(FboApplicationPayment fboApplicationPayment) {
		this.fboApplicationPayment = fboApplicationPayment;
	}

	public List<FboApplicationPaymentLog> getFboApplicationPaymentLogs() {
		return this.fboApplicationPaymentLogs;
	}

	public void setFboApplicationPaymentLogs(List<FboApplicationPaymentLog> fboApplicationPaymentLogs) {
		this.fboApplicationPaymentLogs = fboApplicationPaymentLogs;
	}

	public FboApplicationPaymentLog addFboApplicationPaymentLog(FboApplicationPaymentLog fboApplicationPaymentLog) {
		getFboApplicationPaymentLogs().add(fboApplicationPaymentLog);
		fboApplicationPaymentLog.setFboApplicationDetail(this);

		return fboApplicationPaymentLog;
	}

	public FboApplicationPaymentLog removeFboApplicationPaymentLog(FboApplicationPaymentLog fboApplicationPaymentLog) {
		getFboApplicationPaymentLogs().remove(fboApplicationPaymentLog);
		fboApplicationPaymentLog.setFboApplicationDetail(null);

		return fboApplicationPaymentLog;
	}

	public List<FboApplicationProcessLog> getFboApplicationProcessLogs() {
		return this.fboApplicationProcessLogs;
	}

	public void setFboApplicationProcessLogs(List<FboApplicationProcessLog> fboApplicationProcessLogs) {
		this.fboApplicationProcessLogs = fboApplicationProcessLogs;
	}

	public FboApplicationProcessLog addFboApplicationProcessLog(FboApplicationProcessLog fboApplicationProcessLog) {
		getFboApplicationProcessLogs().add(fboApplicationProcessLog);
		fboApplicationProcessLog.setFboApplicationDetail(this);

		return fboApplicationProcessLog;
	}

	public FboApplicationProcessLog removeFboApplicationProcessLog(FboApplicationProcessLog fboApplicationProcessLog) {
		getFboApplicationProcessLogs().remove(fboApplicationProcessLog);
		fboApplicationProcessLog.setFboApplicationDetail(null);

		return fboApplicationProcessLog;
	}

	public List<MasterPaymentMode> getMasterPaymentModes() {
		return this.masterPaymentModes;
	}

	public void setMasterPaymentModes(List<MasterPaymentMode> masterPaymentModes) {
		this.masterPaymentModes = masterPaymentModes;
	}

	public MasterPaymentMode addMasterPaymentMode(MasterPaymentMode masterPaymentMode) {
		getMasterPaymentModes().add(masterPaymentMode);
		masterPaymentMode.setFboApplicationDetail(this);

		return masterPaymentMode;
	}

	public MasterPaymentMode removeMasterPaymentMode(MasterPaymentMode masterPaymentMode) {
		getMasterPaymentModes().remove(masterPaymentMode);
		masterPaymentMode.setFboApplicationDetail(null);

		return masterPaymentMode;
	}

}
