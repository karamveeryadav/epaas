package com.epaas.entities;
import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the fbo_application_ingradients database table.
 * 
 */
@Entity
@Table(name="fbo_application_ingradients")
@NamedQuery(name="FboApplicationIngradient.findAll", query="SELECT f FROM FboApplicationIngradient f")
public class FboApplicationIngradient implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="fbo_application_ingradients_id", unique=true, nullable=false)
	private Integer fboApplicationIngradientsId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="fbo_application_ingradients_name", length=100)
	private String fboApplicationIngradientsName;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to FboApplicationDetail
	@ManyToOne
	@JoinColumn(name="ref_id", nullable=false)
	private FboApplicationDetail fboApplicationDetail;

	public FboApplicationIngradient() {
	}

	public Integer getFboApplicationIngradientsId() {
		return this.fboApplicationIngradientsId;
	}

	public void setFboApplicationIngradientsId(Integer fboApplicationIngradientsId) {
		this.fboApplicationIngradientsId = fboApplicationIngradientsId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getFboApplicationIngradientsName() {
		return this.fboApplicationIngradientsName;
	}

	public void setFboApplicationIngradientsName(String fboApplicationIngradientsName) {
		this.fboApplicationIngradientsName = fboApplicationIngradientsName;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public FboApplicationDetail getFboApplicationDetail() {
		return this.fboApplicationDetail;
	}

	public void setFboApplicationDetail(FboApplicationDetail fboApplicationDetail) {
		this.fboApplicationDetail = fboApplicationDetail;
	}

}