package com.epaas.entities;
import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the fbo_application_payment database table.
 * 
 */
@Entity
@Table(name="fbo_application_payment")
@NamedQuery(name="FboApplicationPayment.findAll", query="SELECT f FROM FboApplicationPayment f")
public class FboApplicationPayment implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ref_id", unique=true, nullable=false)
	private Integer refId;

	@Column(precision=18, scale=2)
	private BigDecimal amount;

	@Column(name="confirm_fee_by")
	private Long confirmFeeBy;

	@Temporal(TemporalType.DATE)
	@Column(name="confirm_fee_date")
	private Date confirmFeeDate;

	@Column(name="confrim_fee")
	private Boolean confrimFee;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="late_fees", precision=18, scale=2)
	private BigDecimal lateFees;

	@Column(name="no_of_days")
	private Integer noOfDays;

	@Column(name="payment_number", length=50)
	private String paymentNumber;

	@Column(name="payment_status")
	private Boolean paymentStatus;

	@Column(name="postal_charges", precision=18, scale=2)
	private BigDecimal postalCharges;

	@Column(name="refund_amount", precision=18, scale=2)
	private BigDecimal refundAmount;

	@Column(name="refund_date")
	private Timestamp refundDate;

	@Column(name="total_gst_amount", precision=18, scale=2)
	private BigDecimal totalGstAmount;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional one-to-one association to FboApplicationDetail
	@OneToOne
	@JoinColumn(name="ref_id", nullable=false, insertable=false, updatable=false)
	private FboApplicationDetail fboApplicationDetail;

	//bi-directional many-to-one association to MasterPaymentMode
	@ManyToOne
	@JoinColumn(name="payment_mode_id")
	private MasterPaymentMode masterPaymentMode;

	public FboApplicationPayment() {
	}

	public Integer getRefId() {
		return this.refId;
	}

	public void setRefId(Integer refId) {
		this.refId = refId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Long getConfirmFeeBy() {
		return this.confirmFeeBy;
	}

	public void setConfirmFeeBy(Long confirmFeeBy) {
		this.confirmFeeBy = confirmFeeBy;
	}

	public Date getConfirmFeeDate() {
		return this.confirmFeeDate;
	}

	public void setConfirmFeeDate(Date confirmFeeDate) {
		this.confirmFeeDate = confirmFeeDate;
	}

	public Boolean getConfrimFee() {
		return this.confrimFee;
	}

	public void setConfrimFee(Boolean confrimFee) {
		this.confrimFee = confrimFee;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public BigDecimal getLateFees() {
		return this.lateFees;
	}

	public void setLateFees(BigDecimal lateFees) {
		this.lateFees = lateFees;
	}

	public Integer getNoOfDays() {
		return this.noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public String getPaymentNumber() {
		return this.paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public Boolean getPaymentStatus() {
		return this.paymentStatus;
	}

	public void setPaymentStatus(Boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public BigDecimal getPostalCharges() {
		return this.postalCharges;
	}

	public void setPostalCharges(BigDecimal postalCharges) {
		this.postalCharges = postalCharges;
	}

	public BigDecimal getRefundAmount() {
		return this.refundAmount;
	}

	public void setRefundAmount(BigDecimal refundAmount) {
		this.refundAmount = refundAmount;
	}

	public Timestamp getRefundDate() {
		return this.refundDate;
	}

	public void setRefundDate(Timestamp refundDate) {
		this.refundDate = refundDate;
	}

	public BigDecimal getTotalGstAmount() {
		return this.totalGstAmount;
	}

	public void setTotalGstAmount(BigDecimal totalGstAmount) {
		this.totalGstAmount = totalGstAmount;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public FboApplicationDetail getFboApplicationDetail() {
		return this.fboApplicationDetail;
	}

	public void setFboApplicationDetail(FboApplicationDetail fboApplicationDetail) {
		this.fboApplicationDetail = fboApplicationDetail;
	}

	public MasterPaymentMode getMasterPaymentMode() {
		return this.masterPaymentMode;
	}

	public void setMasterPaymentMode(MasterPaymentMode masterPaymentMode) {
		this.masterPaymentMode = masterPaymentMode;
	}

}