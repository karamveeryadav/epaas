package com.epaas.entities;
import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.Timestamp;


/**
 * The persistent class for the fbo_application_payment_log database table.
 * 
 */
@Entity
@Table(name="fbo_application_payment_log")
@NamedQuery(name="FboApplicationPaymentLog.findAll", query="SELECT f FROM FboApplicationPaymentLog f")
public class FboApplicationPaymentLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="merchant_trn_id", unique=true, nullable=false, length=50)
	private String merchantTrnId;

	@Column(precision=18, scale=2)
	private BigDecimal amount;

	@Column(name="cgst_amount", precision=18, scale=2)
	private BigDecimal cgstAmount;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="etreasury_grn", length=50)
	private String etreasuryGrn;

	@Column(name="fbo_gst_no", length=50)
	private String fboGstNo;

	@Column(name="fssai_gst_no", length=50)
	private String fssaiGstNo;

	@Column(name="hsn_no", length=50)
	private String hsnNo;

	@Column(name="igst_amount", precision=18, scale=2)
	private BigDecimal igstAmount;

	@Column(name="late_fees", precision=18, scale=2)
	private BigDecimal lateFees;

	@Column(name="no_of_days")
	private Integer noOfDays;

	@Temporal(TemporalType.DATE)
	@Column(name="payment_date")
	private Date paymentDate;

	@Column(name="payment_flag", length=50)
	private String paymentFlag;

	@Column(name="payment_status")
	private Boolean paymentStatus;

	@Column(name="pg_error", length=200)
	private String pgError;

	@Column(name="pg_transaction_id", length=50)
	private String pgTransactionId;

	@Column(name="process_flag")
	private Boolean processFlag;

	@Column(name="razorpay_order_id", length=50)
	private String razorpayOrderId;

	@Column(name="refund_by")
	private Long refundBy;

	@Column(name="refund_date")
	private Timestamp refundDate;

	@Column(name="refund_remark", length=2147483647)
	private String refundRemark;

	@Column(name="refund_request_id", length=50)
	private String refundRequestId;

	@Column(name="refund_status", length=50)
	private String refundStatus;

	@Column(name="response_code", length=50)
	private String responseCode;

	@Column(name="sbi_atrn_number", length=50)
	private String sbiAtrnNumber;

	@Column(name="sgst_ugst_amount", precision=18, scale=2)
	private BigDecimal sgstUgstAmount;

	@Column(name="total_gst_amount", precision=18, scale=2)
	private BigDecimal totalGstAmount;

	@Column(name="union_territory_flag")
	private Boolean unionTerritoryFlag;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to FboApplicationDetail
	@ManyToOne
	@JoinColumn(name="ref_id")
	private FboApplicationDetail fboApplicationDetail;

	//bi-directional many-to-one association to MasterPaymentMode
	@ManyToOne
	@JoinColumn(name="payment_mode_id")
	private MasterPaymentMode masterPaymentMode;

	public FboApplicationPaymentLog() {
	}

	public String getMerchantTrnId() {
		return this.merchantTrnId;
	}

	public void setMerchantTrnId(String merchantTrnId) {
		this.merchantTrnId = merchantTrnId;
	}

	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public BigDecimal getCgstAmount() {
		return this.cgstAmount;
	}

	public void setCgstAmount(BigDecimal cgstAmount) {
		this.cgstAmount = cgstAmount;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getEtreasuryGrn() {
		return this.etreasuryGrn;
	}

	public void setEtreasuryGrn(String etreasuryGrn) {
		this.etreasuryGrn = etreasuryGrn;
	}

	public String getFboGstNo() {
		return this.fboGstNo;
	}

	public void setFboGstNo(String fboGstNo) {
		this.fboGstNo = fboGstNo;
	}

	public String getFssaiGstNo() {
		return this.fssaiGstNo;
	}

	public void setFssaiGstNo(String fssaiGstNo) {
		this.fssaiGstNo = fssaiGstNo;
	}

	public String getHsnNo() {
		return this.hsnNo;
	}

	public void setHsnNo(String hsnNo) {
		this.hsnNo = hsnNo;
	}

	public BigDecimal getIgstAmount() {
		return this.igstAmount;
	}

	public void setIgstAmount(BigDecimal igstAmount) {
		this.igstAmount = igstAmount;
	}

	public BigDecimal getLateFees() {
		return this.lateFees;
	}

	public void setLateFees(BigDecimal lateFees) {
		this.lateFees = lateFees;
	}

	public Integer getNoOfDays() {
		return this.noOfDays;
	}

	public void setNoOfDays(Integer noOfDays) {
		this.noOfDays = noOfDays;
	}

	public Date getPaymentDate() {
		return this.paymentDate;
	}

	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getPaymentFlag() {
		return this.paymentFlag;
	}

	public void setPaymentFlag(String paymentFlag) {
		this.paymentFlag = paymentFlag;
	}

	public Boolean getPaymentStatus() {
		return this.paymentStatus;
	}

	public void setPaymentStatus(Boolean paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPgError() {
		return this.pgError;
	}

	public void setPgError(String pgError) {
		this.pgError = pgError;
	}

	public String getPgTransactionId() {
		return this.pgTransactionId;
	}

	public void setPgTransactionId(String pgTransactionId) {
		this.pgTransactionId = pgTransactionId;
	}

	public Boolean getProcessFlag() {
		return this.processFlag;
	}

	public void setProcessFlag(Boolean processFlag) {
		this.processFlag = processFlag;
	}

	public String getRazorpayOrderId() {
		return this.razorpayOrderId;
	}

	public void setRazorpayOrderId(String razorpayOrderId) {
		this.razorpayOrderId = razorpayOrderId;
	}

	public Long getRefundBy() {
		return this.refundBy;
	}

	public void setRefundBy(Long refundBy) {
		this.refundBy = refundBy;
	}

	public Timestamp getRefundDate() {
		return this.refundDate;
	}

	public void setRefundDate(Timestamp refundDate) {
		this.refundDate = refundDate;
	}

	public String getRefundRemark() {
		return this.refundRemark;
	}

	public void setRefundRemark(String refundRemark) {
		this.refundRemark = refundRemark;
	}

	public String getRefundRequestId() {
		return this.refundRequestId;
	}

	public void setRefundRequestId(String refundRequestId) {
		this.refundRequestId = refundRequestId;
	}

	public String getRefundStatus() {
		return this.refundStatus;
	}

	public void setRefundStatus(String refundStatus) {
		this.refundStatus = refundStatus;
	}

	public String getResponseCode() {
		return this.responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getSbiAtrnNumber() {
		return this.sbiAtrnNumber;
	}

	public void setSbiAtrnNumber(String sbiAtrnNumber) {
		this.sbiAtrnNumber = sbiAtrnNumber;
	}

	public BigDecimal getSgstUgstAmount() {
		return this.sgstUgstAmount;
	}

	public void setSgstUgstAmount(BigDecimal sgstUgstAmount) {
		this.sgstUgstAmount = sgstUgstAmount;
	}

	public BigDecimal getTotalGstAmount() {
		return this.totalGstAmount;
	}

	public void setTotalGstAmount(BigDecimal totalGstAmount) {
		this.totalGstAmount = totalGstAmount;
	}

	public Boolean getUnionTerritoryFlag() {
		return this.unionTerritoryFlag;
	}

	public void setUnionTerritoryFlag(Boolean unionTerritoryFlag) {
		this.unionTerritoryFlag = unionTerritoryFlag;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public FboApplicationDetail getFboApplicationDetail() {
		return this.fboApplicationDetail;
	}

	public void setFboApplicationDetail(FboApplicationDetail fboApplicationDetail) {
		this.fboApplicationDetail = fboApplicationDetail;
	}

	public MasterPaymentMode getMasterPaymentMode() {
		return this.masterPaymentMode;
	}

	public void setMasterPaymentMode(MasterPaymentMode masterPaymentMode) {
		this.masterPaymentMode = masterPaymentMode;
	}

}