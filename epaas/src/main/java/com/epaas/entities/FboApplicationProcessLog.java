package com.epaas.entities;
import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the fbo_application_process_log database table.
 * 
 */
@Entity
@Table(name="fbo_application_process_log")
@NamedQuery(name="FboApplicationProcessLog.findAll", query="SELECT f FROM FboApplicationProcessLog f")
public class FboApplicationProcessLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="log_id", unique=true, nullable=false)
	private Integer logId;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="doc_path", length=200)
	private String docPath;

	@Column(name="processed_by")
	private Long processedBy;

	@Column(name="processed_by_name", length=500)
	private String processedByName;

	@Column(name="processed_name", nullable=false, length=2147483647)
	private String processedName;

	@Column(name="processed_on")
	private Timestamp processedOn;

	@Column(length=5000)
	private String remarks;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to FboApplicationDetail
	@ManyToOne
	@JoinColumn(name="ref_id", nullable=false)
	private FboApplicationDetail fboApplicationDetail;

	//bi-directional many-to-one association to MasterStatus
	@ManyToOne
	@JoinColumn(name="pre_status_id")
	private MasterStatus masterStatus1;

	//bi-directional many-to-one association to MasterStatus
	@ManyToOne
	@JoinColumn(name="status_id", nullable=false)
	private MasterStatus masterStatus2;

	public FboApplicationProcessLog() {
	}

	public Integer getLogId() {
		return this.logId;
	}

	public void setLogId(Integer logId) {
		this.logId = logId;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getDocPath() {
		return this.docPath;
	}

	public void setDocPath(String docPath) {
		this.docPath = docPath;
	}

	public Long getProcessedBy() {
		return this.processedBy;
	}

	public void setProcessedBy(Long processedBy) {
		this.processedBy = processedBy;
	}

	public String getProcessedByName() {
		return this.processedByName;
	}

	public void setProcessedByName(String processedByName) {
		this.processedByName = processedByName;
	}

	public String getProcessedName() {
		return this.processedName;
	}

	public void setProcessedName(String processedName) {
		this.processedName = processedName;
	}

	public Timestamp getProcessedOn() {
		return this.processedOn;
	}

	public void setProcessedOn(Timestamp processedOn) {
		this.processedOn = processedOn;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public FboApplicationDetail getFboApplicationDetail() {
		return this.fboApplicationDetail;
	}

	public void setFboApplicationDetail(FboApplicationDetail fboApplicationDetail) {
		this.fboApplicationDetail = fboApplicationDetail;
	}

	public MasterStatus getMasterStatus1() {
		return this.masterStatus1;
	}

	public void setMasterStatus1(MasterStatus masterStatus1) {
		this.masterStatus1 = masterStatus1;
	}

	public MasterStatus getMasterStatus2() {
		return this.masterStatus2;
	}

	public void setMasterStatus2(MasterStatus masterStatus2) {
		this.masterStatus2 = masterStatus2;
	}

}