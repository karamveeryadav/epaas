package com.epaas.entities;
import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the master_additive database table.
 * 
 */
@Entity
@Table(name="master_additive")
@NamedQuery(name="MasterAdditive.findAll", query="SELECT m FROM MasterAdditive m")
public class MasterAdditive implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="additive_id", unique=true, nullable=false)
	private Integer additiveId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="additive_name", length=100)
	private String additiveName;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	public MasterAdditive() {
	}

	public Integer getAdditiveId() {
		return this.additiveId;
	}

	public void setAdditiveId(Integer additiveId) {
		this.additiveId = additiveId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getAdditiveName() {
		return this.additiveName;
	}

	public void setAdditiveName(String additiveName) {
		this.additiveName = additiveName;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

}