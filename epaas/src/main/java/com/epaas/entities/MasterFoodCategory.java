package com.epaas.entities;
import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the master_food_category database table.
 * 
 */
@Entity
@Table(name="master_food_category")
@NamedQuery(name="MasterFoodCategory.findAll", query="SELECT m FROM MasterFoodCategory m")
public class MasterFoodCategory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="food_category_id", unique=true, nullable=false)
	private Integer foodCategoryId;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="food_descr", length=2147483647)
	private String foodDescr;

	@Column(name="index_val", length=50)
	private String indexVal;

	private Integer level;

	@Column(length=500)
	private String name;

	@Column(name="order_val")
	private Integer orderVal;

	@Column(length=10)
	private String status;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	public MasterFoodCategory() {
	}

	public Integer getFoodCategoryId() {
		return this.foodCategoryId;
	}

	public void setFoodCategoryId(Integer foodCategoryId) {
		this.foodCategoryId = foodCategoryId;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getFoodDescr() {
		return this.foodDescr;
	}

	public void setFoodDescr(String foodDescr) {
		this.foodDescr = foodDescr;
	}

	public String getIndexVal() {
		return this.indexVal;
	}

	public void setIndexVal(String indexVal) {
		this.indexVal = indexVal;
	}

	public Integer getLevel() {
		return this.level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getOrderVal() {
		return this.orderVal;
	}

	public void setOrderVal(Integer orderVal) {
		this.orderVal = orderVal;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

}