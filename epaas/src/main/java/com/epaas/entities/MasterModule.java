package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the master_module database table.
 * 
 */
@Entity
@Table(name="master_module")
@NamedQuery(name="MasterModule.findAll", query="SELECT m FROM MasterModule m")
public class MasterModule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="module_id", unique=true, nullable=false)
	private Integer moduleId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by", nullable=false)
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="module_name", nullable=false, length=200)
	private String moduleName;

	@Column(name="module_url", nullable=false, length=200)
	private String moduleUrl;

	@Column(name="order_val")
	private Integer orderVal;

	@Column(name="updated_by", nullable=false)
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to MasterSubModule
	@OneToMany(mappedBy="masterModule")
	private List<MasterSubModule> masterSubModules;

	//bi-directional many-to-one association to MasterUserVsSubModule
	@OneToMany(mappedBy="masterModule")
	private List<MasterUserVsSubModule> masterUserVsSubModules;

	public MasterModule() {
	}

	public Integer getModuleId() {
		return this.moduleId;
	}

	public void setModuleId(Integer moduleId) {
		this.moduleId = moduleId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getModuleName() {
		return this.moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getModuleUrl() {
		return this.moduleUrl;
	}

	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}

	public Integer getOrderVal() {
		return this.orderVal;
	}

	public void setOrderVal(Integer orderVal) {
		this.orderVal = orderVal;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public List<MasterSubModule> getMasterSubModules() {
		return this.masterSubModules;
	}

	public void setMasterSubModules(List<MasterSubModule> masterSubModules) {
		this.masterSubModules = masterSubModules;
	}

	public MasterSubModule addMasterSubModule(MasterSubModule masterSubModule) {
		getMasterSubModules().add(masterSubModule);
		masterSubModule.setMasterModule(this);

		return masterSubModule;
	}

	public MasterSubModule removeMasterSubModule(MasterSubModule masterSubModule) {
		getMasterSubModules().remove(masterSubModule);
		masterSubModule.setMasterModule(null);

		return masterSubModule;
	}

	public List<MasterUserVsSubModule> getMasterUserVsSubModules() {
		return this.masterUserVsSubModules;
	}

	public void setMasterUserVsSubModules(List<MasterUserVsSubModule> masterUserVsSubModules) {
		this.masterUserVsSubModules = masterUserVsSubModules;
	}

	public MasterUserVsSubModule addMasterUserVsSubModule(MasterUserVsSubModule masterUserVsSubModule) {
		getMasterUserVsSubModules().add(masterUserVsSubModule);
		masterUserVsSubModule.setMasterModule(this);

		return masterUserVsSubModule;
	}

	public MasterUserVsSubModule removeMasterUserVsSubModule(MasterUserVsSubModule masterUserVsSubModule) {
		getMasterUserVsSubModules().remove(masterUserVsSubModule);
		masterUserVsSubModule.setMasterModule(null);

		return masterUserVsSubModule;
	}

}