package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the master_payment_mode database table.
 * 
 */
@Entity
@Table(name="master_payment_mode")
@NamedQuery(name="MasterPaymentMode.findAll", query="SELECT m FROM MasterPaymentMode m")
public class MasterPaymentMode implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="payment_mode_id", unique=true, nullable=false)
	private Integer paymentModeId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="base_url_backend", length=200)
	private String baseUrlBackend;

	@Column(name="base_url_frontend", length=200)
	private String baseUrlFrontend;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="flrs_payment_mode_name", length=100)
	private String flrsPaymentModeName;

	@Column(name="order_val")
	private Integer orderVal;

	@Column(name="payment_mode_desc", length=500)
	private String paymentModeDesc;

	@Column(name="payment_mode_name", length=100)
	private String paymentModeName;

	@Column(name="payment_url", length=500)
	private String paymentUrl;

	@Column(name="payubiz_curl", length=200)
	private String payubizCurl;

	@Column(name="payubiz_furl", length=200)
	private String payubizFurl;

	@Column(name="payubiz_key", length=50)
	private String payubizKey;

	@Column(name="payubiz_salt", length=50)
	private String payubizSalt;

	@Column(name="payubiz_surl", length=200)
	private String payubizSurl;

	@Column(name="razorpay_cancel_url", length=200)
	private String razorpayCancelUrl;

	@Column(name="razorpay_fail_url", length=200)
	private String razorpayFailUrl;

	@Column(name="razorpay_key_id", length=50)
	private String razorpayKeyId;

	@Column(name="razorpay_key_secret", length=50)
	private String razorpayKeySecret;

	@Column(name="razorpay_order_url", length=200)
	private String razorpayOrderUrl;

	@Column(name="razorpay_success_url", length=200)
	private String razorpaySuccessUrl;

	@Column(name="razorpay_webhooksecret", length=50)
	private String razorpayWebhooksecret;

	@Column(name="refund_url", length=300)
	private String refundUrl;

	@Column(name="state_code", length=4)
	private String stateCode;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	@Column(name="verification_url", length=300)
	private String verificationUrl;

	//bi-directional many-to-one association to FboApplicationPayment
	@OneToMany(mappedBy="masterPaymentMode")
	private List<FboApplicationPayment> fboApplicationPayments;

	//bi-directional many-to-one association to FboApplicationPaymentLog
	@OneToMany(mappedBy="masterPaymentMode")
	private List<FboApplicationPaymentLog> fboApplicationPaymentLogs;

	//bi-directional many-to-one association to FboApplicationDetail
	@ManyToOne
	@JoinColumn(name="ref_id", nullable=false)
	private FboApplicationDetail fboApplicationDetail;

	public MasterPaymentMode() {
	}

	public Integer getPaymentModeId() {
		return this.paymentModeId;
	}

	public void setPaymentModeId(Integer paymentModeId) {
		this.paymentModeId = paymentModeId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public String getBaseUrlBackend() {
		return this.baseUrlBackend;
	}

	public void setBaseUrlBackend(String baseUrlBackend) {
		this.baseUrlBackend = baseUrlBackend;
	}

	public String getBaseUrlFrontend() {
		return this.baseUrlFrontend;
	}

	public void setBaseUrlFrontend(String baseUrlFrontend) {
		this.baseUrlFrontend = baseUrlFrontend;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getFlrsPaymentModeName() {
		return this.flrsPaymentModeName;
	}

	public void setFlrsPaymentModeName(String flrsPaymentModeName) {
		this.flrsPaymentModeName = flrsPaymentModeName;
	}

	public Integer getOrderVal() {
		return this.orderVal;
	}

	public void setOrderVal(Integer orderVal) {
		this.orderVal = orderVal;
	}

	public String getPaymentModeDesc() {
		return this.paymentModeDesc;
	}

	public void setPaymentModeDesc(String paymentModeDesc) {
		this.paymentModeDesc = paymentModeDesc;
	}

	public String getPaymentModeName() {
		return this.paymentModeName;
	}

	public void setPaymentModeName(String paymentModeName) {
		this.paymentModeName = paymentModeName;
	}

	public String getPaymentUrl() {
		return this.paymentUrl;
	}

	public void setPaymentUrl(String paymentUrl) {
		this.paymentUrl = paymentUrl;
	}

	public String getPayubizCurl() {
		return this.payubizCurl;
	}

	public void setPayubizCurl(String payubizCurl) {
		this.payubizCurl = payubizCurl;
	}

	public String getPayubizFurl() {
		return this.payubizFurl;
	}

	public void setPayubizFurl(String payubizFurl) {
		this.payubizFurl = payubizFurl;
	}

	public String getPayubizKey() {
		return this.payubizKey;
	}

	public void setPayubizKey(String payubizKey) {
		this.payubizKey = payubizKey;
	}

	public String getPayubizSalt() {
		return this.payubizSalt;
	}

	public void setPayubizSalt(String payubizSalt) {
		this.payubizSalt = payubizSalt;
	}

	public String getPayubizSurl() {
		return this.payubizSurl;
	}

	public void setPayubizSurl(String payubizSurl) {
		this.payubizSurl = payubizSurl;
	}

	public String getRazorpayCancelUrl() {
		return this.razorpayCancelUrl;
	}

	public void setRazorpayCancelUrl(String razorpayCancelUrl) {
		this.razorpayCancelUrl = razorpayCancelUrl;
	}

	public String getRazorpayFailUrl() {
		return this.razorpayFailUrl;
	}

	public void setRazorpayFailUrl(String razorpayFailUrl) {
		this.razorpayFailUrl = razorpayFailUrl;
	}

	public String getRazorpayKeyId() {
		return this.razorpayKeyId;
	}

	public void setRazorpayKeyId(String razorpayKeyId) {
		this.razorpayKeyId = razorpayKeyId;
	}

	public String getRazorpayKeySecret() {
		return this.razorpayKeySecret;
	}

	public void setRazorpayKeySecret(String razorpayKeySecret) {
		this.razorpayKeySecret = razorpayKeySecret;
	}

	public String getRazorpayOrderUrl() {
		return this.razorpayOrderUrl;
	}

	public void setRazorpayOrderUrl(String razorpayOrderUrl) {
		this.razorpayOrderUrl = razorpayOrderUrl;
	}

	public String getRazorpaySuccessUrl() {
		return this.razorpaySuccessUrl;
	}

	public void setRazorpaySuccessUrl(String razorpaySuccessUrl) {
		this.razorpaySuccessUrl = razorpaySuccessUrl;
	}

	public String getRazorpayWebhooksecret() {
		return this.razorpayWebhooksecret;
	}

	public void setRazorpayWebhooksecret(String razorpayWebhooksecret) {
		this.razorpayWebhooksecret = razorpayWebhooksecret;
	}

	public String getRefundUrl() {
		return this.refundUrl;
	}

	public void setRefundUrl(String refundUrl) {
		this.refundUrl = refundUrl;
	}

	public String getStateCode() {
		return this.stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getVerificationUrl() {
		return this.verificationUrl;
	}

	public void setVerificationUrl(String verificationUrl) {
		this.verificationUrl = verificationUrl;
	}

	public List<FboApplicationPayment> getFboApplicationPayments() {
		return this.fboApplicationPayments;
	}

	public void setFboApplicationPayments(List<FboApplicationPayment> fboApplicationPayments) {
		this.fboApplicationPayments = fboApplicationPayments;
	}

	public FboApplicationPayment addFboApplicationPayment(FboApplicationPayment fboApplicationPayment) {
		getFboApplicationPayments().add(fboApplicationPayment);
		fboApplicationPayment.setMasterPaymentMode(this);

		return fboApplicationPayment;
	}

	public FboApplicationPayment removeFboApplicationPayment(FboApplicationPayment fboApplicationPayment) {
		getFboApplicationPayments().remove(fboApplicationPayment);
		fboApplicationPayment.setMasterPaymentMode(null);

		return fboApplicationPayment;
	}

	public List<FboApplicationPaymentLog> getFboApplicationPaymentLogs() {
		return this.fboApplicationPaymentLogs;
	}

	public void setFboApplicationPaymentLogs(List<FboApplicationPaymentLog> fboApplicationPaymentLogs) {
		this.fboApplicationPaymentLogs = fboApplicationPaymentLogs;
	}

	public FboApplicationPaymentLog addFboApplicationPaymentLog(FboApplicationPaymentLog fboApplicationPaymentLog) {
		getFboApplicationPaymentLogs().add(fboApplicationPaymentLog);
		fboApplicationPaymentLog.setMasterPaymentMode(this);

		return fboApplicationPaymentLog;
	}

	public FboApplicationPaymentLog removeFboApplicationPaymentLog(FboApplicationPaymentLog fboApplicationPaymentLog) {
		getFboApplicationPaymentLogs().remove(fboApplicationPaymentLog);
		fboApplicationPaymentLog.setMasterPaymentMode(null);

		return fboApplicationPaymentLog;
	}

	public FboApplicationDetail getFboApplicationDetail() {
		return this.fboApplicationDetail;
	}

	public void setFboApplicationDetail(FboApplicationDetail fboApplicationDetail) {
		this.fboApplicationDetail = fboApplicationDetail;
	}

}