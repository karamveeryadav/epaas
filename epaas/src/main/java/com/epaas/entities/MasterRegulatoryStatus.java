package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the master_regulatory_status database table.
 * 
 */
@Entity
@Table(name="master_regulatory_status")
@NamedQuery(name="MasterRegulatoryStatus.findAll", query="SELECT m FROM MasterRegulatoryStatus m")
public class MasterRegulatoryStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="regulatory_status_id", unique=true, nullable=false)
	private Integer regulatoryStatusId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="regulatory_status_name", length=1000)
	private String regulatoryStatusName;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	public MasterRegulatoryStatus() {
	}

	public Integer getRegulatoryStatusId() {
		return this.regulatoryStatusId;
	}

	public void setRegulatoryStatusId(Integer regulatoryStatusId) {
		this.regulatoryStatusId = regulatoryStatusId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getRegulatoryStatusName() {
		return this.regulatoryStatusName;
	}

	public void setRegulatoryStatusName(String regulatoryStatusName) {
		this.regulatoryStatusName = regulatoryStatusName;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

}