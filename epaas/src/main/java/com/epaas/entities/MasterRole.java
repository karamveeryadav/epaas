package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the master_roles database table.
 * 
 */
@Entity
@Table(name="master_roles")
@NamedQuery(name="MasterRole.findAll", query="SELECT m FROM MasterRole m")
public class MasterRole implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="role_id", unique=true, nullable=false)
	private Integer roleId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by", nullable=false)
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="role_desc", length=200)
	private String roleDesc;

	@Column(name="role_name", nullable=false, length=200)
	private String roleName;

	@Column(name="updated_by", nullable=false)
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to MasterUser
	@OneToMany(mappedBy="masterRole")
	private List<MasterUser> masterUser;

	//bi-directional many-to-one association to MasterUserVsSubModule
	@OneToMany(mappedBy="masterRole")
	private List<MasterUserVsSubModule> masterUserVsSubModules;

	public MasterRole() {
	}

	public Integer getRoleId() {
		return this.roleId;
	}

	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getRoleDesc() {
		return this.roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public String getRoleName() {
		return this.roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}
//
//	public List<MasterUser> getMasterUsers() {
//		return this.masterUser;
//	}
//
//	public void setMasterUsers(List<MasterUser> masterUsers) {
//		this.masterUser = masterUsers;
//	}

//	public MasterUser addMasterUser(MasterUser masterUser) {
//		getMasterUsers().add(masterUser);
//		masterUser.setMasterRole(this);
//
//		return masterUser;
//	}
//
//	public MasterUser removeMasterUser(MasterUser masterUser) {
//		getMasterUsers().remove(masterUser);
//		masterUser.setMasterRole(null);
//
//		return masterUser;
//	}

	public List<MasterUserVsSubModule> getMasterUserVsSubModules() {
		return this.masterUserVsSubModules;
	}

	public void setMasterUserVsSubModules(List<MasterUserVsSubModule> masterUserVsSubModules) {
		this.masterUserVsSubModules = masterUserVsSubModules;
	}

	public MasterUserVsSubModule addMasterUserVsSubModule(MasterUserVsSubModule masterUserVsSubModule) {
		getMasterUserVsSubModules().add(masterUserVsSubModule);
		masterUserVsSubModule.setMasterRole(this);

		return masterUserVsSubModule;
	}

	public MasterUserVsSubModule removeMasterUserVsSubModule(MasterUserVsSubModule masterUserVsSubModule) {
		getMasterUserVsSubModules().remove(masterUserVsSubModule);
		masterUserVsSubModule.setMasterRole(null);

		return masterUserVsSubModule;
	}

}