package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the master_safety_information database table.
 * 
 */
@Entity
@Table(name="master_safety_information")
@NamedQuery(name="MasterSafetyInformation.findAll", query="SELECT m FROM MasterSafetyInformation m")
public class MasterSafetyInformation implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="safety_information_id", unique=true, nullable=false)
	private Integer safetyInformationId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="safety_information_name", length=1000)
	private String safetyInformationName;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	public MasterSafetyInformation() {
	}

	public Integer getSafetyInformationId() {
		return this.safetyInformationId;
	}

	public void setSafetyInformationId(Integer safetyInformationId) {
		this.safetyInformationId = safetyInformationId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getSafetyInformationName() {
		return this.safetyInformationName;
	}

	public void setSafetyInformationName(String safetyInformationName) {
		this.safetyInformationName = safetyInformationName;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

}