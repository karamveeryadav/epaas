package com.epaas.entities;
import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the master_source_of_food_ingradients database table.
 * 
 */
@Entity
@Table(name="master_source_of_food_ingradients")
@NamedQuery(name="MasterSourceOfFoodIngradient.findAll", query="SELECT m FROM MasterSourceOfFoodIngradient m")
public class MasterSourceOfFoodIngradient implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="ingradient_id", unique=true, nullable=false)
	private Integer ingradientId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="ingradient_name", length=500)
	private String ingradientName;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	public MasterSourceOfFoodIngradient() {
	}

	public Integer getIngradientId() {
		return this.ingradientId;
	}

	public void setIngradientId(Integer ingradientId) {
		this.ingradientId = ingradientId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getIngradientName() {
		return this.ingradientName;
	}

	public void setIngradientName(String ingradientName) {
		this.ingradientName = ingradientName;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

}