package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the master_status database table.
 * 
 */
@Entity
@Table(name="master_status")
@NamedQuery(name="MasterStatus.findAll", query="SELECT m FROM MasterStatus m")
public class MasterStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="status_id", unique=true, nullable=false)
	private Integer statusId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by", nullable=false)
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="status_desc", length=100)
	private String statusDesc;

	@Column(name="updated_by", nullable=false)
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to FboApplicationProcessLog
	@OneToMany(mappedBy="masterStatus1")
	private List<FboApplicationProcessLog> fboApplicationProcessLogs1;

	//bi-directional many-to-one association to FboApplicationProcessLog
	@OneToMany(mappedBy="masterStatus2")
	private List<FboApplicationProcessLog> fboApplicationProcessLogs2;

	public MasterStatus() {
	}

	public Integer getStatusId() {
		return this.statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getStatusDesc() {
		return this.statusDesc;
	}

	public void setStatusDesc(String statusDesc) {
		this.statusDesc = statusDesc;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public List<FboApplicationProcessLog> getFboApplicationProcessLogs1() {
		return this.fboApplicationProcessLogs1;
	}

	public void setFboApplicationProcessLogs1(List<FboApplicationProcessLog> fboApplicationProcessLogs1) {
		this.fboApplicationProcessLogs1 = fboApplicationProcessLogs1;
	}

	public FboApplicationProcessLog addFboApplicationProcessLogs1(FboApplicationProcessLog fboApplicationProcessLogs1) {
		getFboApplicationProcessLogs1().add(fboApplicationProcessLogs1);
		fboApplicationProcessLogs1.setMasterStatus1(this);

		return fboApplicationProcessLogs1;
	}

	public FboApplicationProcessLog removeFboApplicationProcessLogs1(FboApplicationProcessLog fboApplicationProcessLogs1) {
		getFboApplicationProcessLogs1().remove(fboApplicationProcessLogs1);
		fboApplicationProcessLogs1.setMasterStatus1(null);

		return fboApplicationProcessLogs1;
	}

	public List<FboApplicationProcessLog> getFboApplicationProcessLogs2() {
		return this.fboApplicationProcessLogs2;
	}

	public void setFboApplicationProcessLogs2(List<FboApplicationProcessLog> fboApplicationProcessLogs2) {
		this.fboApplicationProcessLogs2 = fboApplicationProcessLogs2;
	}

	public FboApplicationProcessLog addFboApplicationProcessLogs2(FboApplicationProcessLog fboApplicationProcessLogs2) {
		getFboApplicationProcessLogs2().add(fboApplicationProcessLogs2);
		fboApplicationProcessLogs2.setMasterStatus2(this);

		return fboApplicationProcessLogs2;
	}

	public FboApplicationProcessLog removeFboApplicationProcessLogs2(FboApplicationProcessLog fboApplicationProcessLogs2) {
		getFboApplicationProcessLogs2().remove(fboApplicationProcessLogs2);
		fboApplicationProcessLogs2.setMasterStatus2(null);

		return fboApplicationProcessLogs2;
	}

}