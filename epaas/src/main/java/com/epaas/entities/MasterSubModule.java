package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the master_sub_module database table.
 * 
 */
@Entity
@Table(name="master_sub_module")
@NamedQuery(name="MasterSubModule.findAll", query="SELECT m FROM MasterSubModule m")
public class MasterSubModule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="sub_module_id", unique=true, nullable=false)
	private Integer subModuleId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by", nullable=false)
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="order_val")
	private Integer orderVal;

	@Column(name="sub_module_name", nullable=false, length=200)
	private String subModuleName;

	@Column(name="sub_module_url", nullable=false, length=200)
	private String subModuleUrl;

	@Column(name="updated_by", nullable=false)
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to MasterModule
	@ManyToOne
	@JoinColumn(name="module_id", nullable=false)
	private MasterModule masterModule;

	//bi-directional many-to-one association to MasterUserVsSubModule
	@OneToMany(mappedBy="masterSubModule")
	private List<MasterUserVsSubModule> masterUserVsSubModules;

	public MasterSubModule() {
	}

	public Integer getSubModuleId() {
		return this.subModuleId;
	}

	public void setSubModuleId(Integer subModuleId) {
		this.subModuleId = subModuleId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getOrderVal() {
		return this.orderVal;
	}

	public void setOrderVal(Integer orderVal) {
		this.orderVal = orderVal;
	}

	public String getSubModuleName() {
		return this.subModuleName;
	}

	public void setSubModuleName(String subModuleName) {
		this.subModuleName = subModuleName;
	}

	public String getSubModuleUrl() {
		return this.subModuleUrl;
	}

	public void setSubModuleUrl(String subModuleUrl) {
		this.subModuleUrl = subModuleUrl;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public MasterModule getMasterModule() {
		return this.masterModule;
	}

	public void setMasterModule(MasterModule masterModule) {
		this.masterModule = masterModule;
	}

	public List<MasterUserVsSubModule> getMasterUserVsSubModules() {
		return this.masterUserVsSubModules;
	}

	public void setMasterUserVsSubModules(List<MasterUserVsSubModule> masterUserVsSubModules) {
		this.masterUserVsSubModules = masterUserVsSubModules;
	}

	public MasterUserVsSubModule addMasterUserVsSubModule(MasterUserVsSubModule masterUserVsSubModule) {
		getMasterUserVsSubModules().add(masterUserVsSubModule);
		masterUserVsSubModule.setMasterSubModule(this);

		return masterUserVsSubModule;
	}

	public MasterUserVsSubModule removeMasterUserVsSubModule(MasterUserVsSubModule masterUserVsSubModule) {
		getMasterUserVsSubModules().remove(masterUserVsSubModule);
		masterUserVsSubModule.setMasterSubModule(null);

		return masterUserVsSubModule;
	}

}