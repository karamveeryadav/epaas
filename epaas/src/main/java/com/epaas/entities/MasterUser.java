package com.epaas.entities;

import java.io.Serializable;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.context.annotation.Configuration;
//@CrossOrigin(origins = "http://localhost:4201")

@Entity
@Table(name="master_user")
@NamedQuery(name="MasterUser.findAll", query="SELECT mu FROM MasterUser mu")
public class MasterUser implements Serializable{
	

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="user_id")
	private int userId;
	
	@Column(name="login_id")
	private int loginId;
	
	@Column(name="user_pwd")
	private String userPwd;
	
	@Column(name="dev_pwd")
	private String devPwd;
	
	@Column(name="company_name")
	private String companyName;
	
	@Column(name="name_of_business")
	private String nameOfBusiness;
	
	@Column(name="license_no")
	private BigInteger licenseNo;
	
	@Column(name="email_id")
	private String emailId;
	
	@Column(name="email_id_verified_flag")
	private boolean emailIdVerifiedFlag; 
	
	@Column(name="mobile_no")
	private BigInteger mobileNo;
	
	@Column(name="mobile_no_verified_flag")
	private boolean mobileNoVerifiedFlag;
	
	@Column(name="user_img_path")
	private String userImgPath;
	
	@Column(name="active_flag")
	private boolean aciveFlag;
	
	@Column(name="role_id")
	private int roleId;
	
	@Column(name="last_login_date_and_time", updatable=false)
	private Timestamp lastLoginDateAndTime = new Timestamp(System.currentTimeMillis());
	
	@Column(name="last_change_password_date_and_time", updatable=false)
	private Timestamp lastChangePasswordDateAndTime = new Timestamp(System.currentTimeMillis());
	
	@Column(name="created_by")
	private BigInteger createdBy; 
	
	@Column(name="created_on", updatable=false)
	private Timestamp createdOn = new Timestamp(System.currentTimeMillis());
	
	@Column(name="updated_by")
	private BigInteger updatedBy;
	
	@Column(name="updated_on", updatable=false)
	private Timestamp updatedOn = new Timestamp(System.currentTimeMillis());
	
	//bi-directional many-to-one association to MasterRole
		@ManyToOne
		@JoinColumn(name="role_id", updatable = false, insertable = false)
		private MasterRole masterRole;

		//bi-directional many-to-one association to UserLoginHistory
		@OneToMany(mappedBy="masterUser")
		private List<UserLoginHistory> userLoginHistories;


	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getLoginId() {
		return loginId;
	}

	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}

	public String getUserPwd() {
		return userPwd;
	}

	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}

	public String getDevPwd() {
		return devPwd;
	}

	public void setDevPwd(String devPwd) {
		this.devPwd = devPwd;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getNameOfBusiness() {
		return nameOfBusiness;
	}

	public void setNameOfBusiness(String nameOfBusiness) {
		this.nameOfBusiness = nameOfBusiness;
	}

	public BigInteger getLicenseNo() {
		return licenseNo;
	}

	public void setLicenseNo(BigInteger licenseNo) {
		this.licenseNo = licenseNo;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public boolean isEmailIdVerifiedFlag() {
		return emailIdVerifiedFlag;
	}

	public void setEmailIdVerifiedFlag(boolean emailIdVerifiedFlag) {
		this.emailIdVerifiedFlag = emailIdVerifiedFlag;
	}

	public BigInteger getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(BigInteger mobileNo) {
		this.mobileNo = mobileNo;
	}

	public boolean isMobileNoVerifiedFlag() {
		return mobileNoVerifiedFlag;
	}

	public void setMobileNoVerifiedFlag(boolean mobileNoVerifiedFlag) {
		this.mobileNoVerifiedFlag = mobileNoVerifiedFlag;
	}

	public String getUserImgPath() {
		return userImgPath;
	}

	public void setUserImgPath(String userImgPath) {
		this.userImgPath = userImgPath;
	}

	public boolean isAciveFlag() {
		return aciveFlag;
	}

	public void setAciveFlag(boolean aciveFlag) {
		this.aciveFlag = aciveFlag;
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public Timestamp getLastLoginDateAndTime() {
		return lastLoginDateAndTime;
	}

	public void setLastLoginDateAndTime(Timestamp lastLoginDateAndTime) {
		this.lastLoginDateAndTime = lastLoginDateAndTime;
	}

	public Timestamp getLastChangePasswordDateAndTime() {
		return lastChangePasswordDateAndTime;
	}

	public void setLastChangePasswordDateAndTime(Timestamp lastChangePasswordDateAndTime) {
		this.lastChangePasswordDateAndTime = lastChangePasswordDateAndTime;
	}

	public BigInteger getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigInteger createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public BigInteger getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(BigInteger updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public MasterRole getMasterRole() {
		return this.masterRole;
	}

	public void setMasterRole(MasterRole masterRole) {
		this.masterRole = masterRole;
	}

	public List<UserLoginHistory> getUserLoginHistories() {
		return this.userLoginHistories;
	}

	public void setUserLoginHistories(List<UserLoginHistory> userLoginHistories) {
		this.userLoginHistories = userLoginHistories;
	}

	public UserLoginHistory addUserLoginHistory(UserLoginHistory userLoginHistory) {
		getUserLoginHistories().add(userLoginHistory);
		userLoginHistory.setMasterUser(this);

		return userLoginHistory;
	}

	public UserLoginHistory removeUserLoginHistory(UserLoginHistory userLoginHistory) {
		getUserLoginHistories().remove(userLoginHistory);
		userLoginHistory.setMasterUser(null);

		return userLoginHistory;
	}

}
