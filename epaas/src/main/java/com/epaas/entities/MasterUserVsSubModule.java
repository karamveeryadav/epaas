package com.epaas.entities;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the master_user_vs_sub_module database table.
 * 
 */
@Entity
@Table(name="master_user_vs_sub_module")
@NamedQuery(name="MasterUserVsSubModule.findAll", query="SELECT m FROM MasterUserVsSubModule m")
public class MasterUserVsSubModule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="map_id", unique=true, nullable=false)
	private Integer mapId;

	@Column(name="active_flag")
	private Boolean activeFlag;

	@Column(name="created_by", nullable=false)
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="order_val")
	private Integer orderVal;

	@Column(name="updated_by", nullable=false)
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to MasterModule
	@ManyToOne
	@JoinColumn(name="module_id", nullable=false)
	private MasterModule masterModule;

	//bi-directional many-to-one association to MasterRole
	@ManyToOne
	@JoinColumn(name="role_id", nullable=false)
	private MasterRole masterRole;

	//bi-directional many-to-one association to MasterSubModule
	@ManyToOne
	@JoinColumn(name="sub_module_id", nullable=false)
	private MasterSubModule masterSubModule;

	public MasterUserVsSubModule() {
	}

	public Integer getMapId() {
		return this.mapId;
	}

	public void setMapId(Integer mapId) {
		this.mapId = mapId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public Integer getOrderVal() {
		return this.orderVal;
	}

	public void setOrderVal(Integer orderVal) {
		this.orderVal = orderVal;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public MasterModule getMasterModule() {
		return this.masterModule;
	}

	public void setMasterModule(MasterModule masterModule) {
		this.masterModule = masterModule;
	}

	public MasterRole getMasterRole() {
		return this.masterRole;
	}

	public void setMasterRole(MasterRole masterRole) {
		this.masterRole = masterRole;
	}

	public MasterSubModule getMasterSubModule() {
		return this.masterSubModule;
	}

	public void setMasterSubModule(MasterSubModule masterSubModule) {
		this.masterSubModule = masterSubModule;
	}

}