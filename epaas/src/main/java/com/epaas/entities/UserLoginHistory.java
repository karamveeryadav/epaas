package com.epaas.entities;
import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;


/**
 * The persistent class for the user_login_history database table.
 * 
 */
@Entity
@Table(name="user_login_history")
@NamedQuery(name="UserLoginHistory.findAll", query="SELECT u FROM UserLoginHistory u")
public class UserLoginHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="login_history_id", unique=true, nullable=false)
	private Integer loginHistoryId;

	@Column(name="active_flag", nullable=false)
	private Boolean activeFlag;

	@Column(name="created_by")
	private Long createdBy;

	@Column(name="created_on")
	private Timestamp createdOn;

	@Column(name="domain_url", nullable=false, length=200)
	private String domainUrl;

	@Column(name="ip_address", nullable=false, length=200)
	private String ipAddress;

	@Column(name="login_id", nullable=false, length=200)
	private String loginId;

	@Column(name="login_status_flag")
	private Boolean loginStatusFlag;

	@Column(name="mac_address", nullable=false, length=200)
	private String macAddress;

	@Column(name="updated_by")
	private Long updatedBy;

	@Column(name="updated_on")
	private Timestamp updatedOn;

	//bi-directional many-to-one association to MasterUser
	@ManyToOne
	@JoinColumn(name="user_id", nullable=false)
	private MasterUser masterUser;

	public UserLoginHistory() {
	}

	public Integer getLoginHistoryId() {
		return this.loginHistoryId;
	}

	public void setLoginHistoryId(Integer loginHistoryId) {
		this.loginHistoryId = loginHistoryId;
	}

	public Boolean getActiveFlag() {
		return this.activeFlag;
	}

	public void setActiveFlag(Boolean activeFlag) {
		this.activeFlag = activeFlag;
	}

	public Long getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(Long createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getDomainUrl() {
		return this.domainUrl;
	}

	public void setDomainUrl(String domainUrl) {
		this.domainUrl = domainUrl;
	}

	public String getIpAddress() {
		return this.ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public Boolean getLoginStatusFlag() {
		return this.loginStatusFlag;
	}

	public void setLoginStatusFlag(Boolean loginStatusFlag) {
		this.loginStatusFlag = loginStatusFlag;
	}

	public String getMacAddress() {
		return this.macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public Long getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(Long updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public MasterUser getMasterUser() {
		return this.masterUser;
	}

	public void setMasterUser(MasterUser masterUser) {
		this.masterUser = masterUser;
	}

}