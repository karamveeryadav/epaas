package com.epaas.services;


import javax.servlet.http.HttpServletResponse;
import com.epaas.bean.OtpMessageSender;
import com.epaas.bean.Status;

public interface CommonService {

	public Status sendOTP(OtpMessageSender otpMessageSender) throws Exception;

	String checkAlreadyExistData(String contactDetails);

	public Status sendOtpForForgotPwd(OtpMessageSender otpMessageSender) throws Exception;

	public void captchaRequest(HttpServletResponse response)throws Exception;
		
		



		
}
