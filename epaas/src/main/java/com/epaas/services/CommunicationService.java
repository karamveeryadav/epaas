package com.epaas.services;

import java.util.Map;

public interface CommunicationService {
	public boolean sendEmails(String emailSubject, String templatePath, String uploadFilePath,Map<String,Object> model,String to,String cc,String bcc);
	public boolean sendSMS(String templatePath,Map<String,Object> model,String to,String templateId);
}
