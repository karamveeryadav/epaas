package com.epaas.services;

import java.util.List;

public interface ReportingServices {

	List<?> getMasterAdditive();

	List<?> getMasterApplicationType();

	List<?> getMasterCopyOfAgreementwithTheEntity();

	List<?> getMasterFoodCategory();

	List<?> getMasterIngredient();

	List<?> getMasterModule();

	List<?> getMasterPaymentMode();

	List<?> getMasterRegulatoryStatus();

	List<?> getMasterRole();

	List<?> getMasterSafetyInformation();

	List<?> getMasterSourceOfFoodIngradient();

	List<?> getMasterStatus();

	List<?> getMasterSubModule();

	List<?> getMasterTypeOfFood();

	List<?> getMasterUserVsSubModule();

}
