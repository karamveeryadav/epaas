package com.epaas.services;

import java.math.BigInteger;
import com.epaas.dto.MasterUserDto;

public interface SignupServices {

	Object getUserDetails(BigInteger userId);
	
	String saveUserDetails(MasterUserDto masterUserDto);
	
	boolean updateUserDetails( MasterUserDto masterUserDto);	

}
