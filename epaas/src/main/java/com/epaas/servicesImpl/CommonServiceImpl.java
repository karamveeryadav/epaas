package com.epaas.servicesImpl;
import java.awt.Color;

import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.epaas.bean.JavaConstant;
import com.epaas.bean.OtpMessageSender;
import com.epaas.bean.Status;
import com.epaas.dao.IGenericDao;
import com.epaas.services.CommonService;
import com.epaas.services.CommunicationService;

@Service
public class CommonServiceImpl implements CommonService{
	
	@Autowired
	IGenericDao igenericDao;
	
	@Autowired
	CommunicationService communicationService;

	@Override
	public Status sendOTP(OtpMessageSender otpMessageSender) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		String otp = new Random().nextInt(999999) + "";
		model.put("otp", otp);
		Status status = new Status();
//		 String flag ="";
		switch (checkAlreadyExistData(otpMessageSender.getTo())) {
		case "200M":
			try {
				communicationService.sendSMS("templates/otpSMS.vm", model, otpMessageSender.getTo(),"1107160960810191572");
//				 flag = "200M";
				status.setGeneratedCode(otp);
				status.setStatusCode("200M");
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case "200E":
			try {
				communicationService.sendEmails("ePAAS - One Time Password - Creation",
						"templates/otpMail.vm", "", model, otpMessageSender.getTo(), "", "");
				status.setGeneratedCode(otp);
				status.setStatusCode("200E");

//				 flag = "200E";
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case "203M":
			status.setStatusCode("203M");
//			 flag = "203M";
			break;
		case "203E":
			status.setStatusCode("203E");
//			 flag = "203E";
			break;
		}
		return status;
	}
	
	@Override
	public String checkAlreadyExistData(String contactDetails) {

		String result = "";

		try {

			Pattern patternMob = Pattern.compile("^\\(?(\\d{3})\\)?[- ]?(\\d{3})[- ]?(\\d{4})$");
			Matcher matcherMob = patternMob.matcher(contactDetails);
			// Make the comparison case-insensitive.
			Pattern patternEmail = Pattern.compile("^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE);
			Matcher matcherEmail = patternEmail.matcher(contactDetails);
			if (matcherMob.matches()) {
				String QUERY_17=" from MasterUser where mobile_no=" + contactDetails;
				if (igenericDao.executeDDLHQL( QUERY_17, new Object[] {  }).size() > 0) {

					result = "203M";
				} else {
					result = "200M";
				}
			}

			if (matcherEmail.matches()) {
				if (igenericDao.executeDDLHQL(JavaConstant.QUERY_16, new Object[] { contactDetails }).size() > 0) {

					result = "203E";
				} else {
					result = "200E";
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
//-----------password encryption-----
	static String getTestingPasswordEncryption(String password) throws Exception{
		return encryptionPassword(password);
	}
	
	static String encryptionPassword(String password) throws InvalidKeyException {
		String   encryptPassword =null;
		try {
			String secret = "$$CHALLENGE";
		    Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
		    sha256_HMAC.init(new SecretKeySpec(secret.getBytes(), "HmacSHA256"));
		    encryptPassword = new String(Base64.encodeBase64(sha256_HMAC.doFinal(password.getBytes())));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return encryptPassword;
	    
	}
	
	@Override
	@Transactional
	public Status sendOtpForForgotPwd(OtpMessageSender otpMessageSender) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		String otp = new Random().nextInt(999999) + "";
		model.put("otp", otp);
		Status status = new Status();
		switch (checkAlreadyExistData(otpMessageSender.getTo())) {
		case "200M":
			status.setStatusCode("203M");
			break;
		case "200E":
			status.setStatusCode("203E");
			break;

		case "203M":
			try {
				communicationService.sendSMS("templates/forgotPasswordSmsOTP.vm", model, otpMessageSender.getTo(),"1107160976324946596");
				status.setGeneratedCode(encryptionPassword(otp));
				status.setStatusCode("200M");
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case "203E":
			try {
				communicationService.sendEmails("ePAAS - One Time Password - Creation",
						"templates/forgotPasswordEmailOTP.vm", "", model, otpMessageSender.getTo(), "", "");
				status.setGeneratedCode(encryptionPassword(otp));
				status.setStatusCode("200E");
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		}
		return status;
	}
	
	@Override
	public void captchaRequest(HttpServletResponse response) throws Exception {
		try {
			int width = 130;
			int height = 40;

			BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			Graphics2D g2d = bufferedImage.createGraphics();
			Font font = new Font("Georgia", Font.BOLD, 18);
			g2d.setFont(font);
			RenderingHints rh = new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			g2d.setRenderingHints(rh);

			GradientPaint gp = new GradientPaint(0, 0, Color.white, 0, height / 2, Color.white, true);
			g2d.setPaint(gp);
			g2d.fillRect(0, 0, width, height);
			g2d.setColor(new Color(0, 0, 0));

			Integer captchaCode = 100000 + new Random().nextInt(800000);
			g2d.drawString(String.valueOf(captchaCode), 27, 27);
			g2d.dispose();
			response.setContentType("image/png");
			OutputStream os = response.getOutputStream();
			response.setHeader("captcha", encryptionPassword(String.valueOf(captchaCode)));

			ImageIO.write(bufferedImage, "png", os);
			os.close();
			response.flushBuffer();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
