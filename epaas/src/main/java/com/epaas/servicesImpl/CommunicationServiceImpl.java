package com.epaas.servicesImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.client.RestTemplate;

import com.epaas.bean.SendViaEmailModel;
import com.epaas.bean.SendViaSMSModel;
import com.epaas.services.CommunicationService;

@Service
public class CommunicationServiceImpl implements CommunicationService {

	@Autowired
	private VelocityEngine velocityEngine;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Override
	public boolean sendEmails(String emailSubject, String templatePath, String uploadFilePath,Map<String,Object> model,String to,String cc,String bcc){
		try {
		SendViaEmailModel mailobj = new SendViaEmailModel();
		mailobj.setToMail(to);
		mailobj.setCcEmail(cc);
		mailobj.setBccEmail(bcc);
		mailobj.setSubject(emailSubject);
		model.put("sSysDate",new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		model.put("sSysTime",new SimpleDateFormat("HH:mm:ss").format(new Date()));
		/*
		Properties prop = new  Properties();
		prop.load(new FileInputStream(new File(getClass().getClassLoader().getResource("templates/foscos_configuration.properties").getFile())));
		*/
		
		model.put("emailid", "karamveeryadav1796@gmail.com");
		model.put("MobileNo", "9918221215");
		model.put("CopyRightName", "ePaas");
		model.put("CopyRightYear", "2022");
		mailobj.setMailContent(VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templatePath, model));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity entity = new HttpEntity(mailobj,headers);
		restTemplate.exchange("http://COMMUNICATION-SERVICE/saveemails", HttpMethod.POST, entity
		   , String.class);
		return true;
		} catch (Exception e) {
		e.printStackTrace();
		return false;
		}
		}
	@Override
	public boolean sendSMS(String templatePath,Map<String,Object> model,String to,String templateId){
		try {
		SendViaSMSModel smsObj = new SendViaSMSModel();
		smsObj.setToSms(to);
		smsObj.setTemplateId(templateId);
		model.put("Name", "ePAAS Team");
		smsObj.setSmsContent(VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, templatePath, model));
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity entity = new HttpEntity(smsObj,headers);
		restTemplate.exchange("http://COMMUNICATION-SERVICE/savesms", HttpMethod.POST, entity, String.class);
		return true;
		} catch (Exception e) {
		e.printStackTrace();
		return false;
		}
		}

}
