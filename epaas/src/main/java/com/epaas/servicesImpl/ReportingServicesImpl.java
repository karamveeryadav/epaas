package com.epaas.servicesImpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.epaas.bean.JavaConstant;
import com.epaas.dao.IGenericDao;
import com.epaas.services.ReportingServices;

@Service
public class ReportingServicesImpl implements ReportingServices {

	@Autowired
	public IGenericDao iGenericDao;
	
	@Override
	public List<?> getMasterAdditive() {
		 return iGenericDao.executeDDLHQL(JavaConstant.QUERY_ADDITIVE, new Object[] {});
	}

	@Override
	public List<?> getMasterApplicationType() {
		 return iGenericDao.executeDDLHQL(JavaConstant.QUERY_MasterApplicationType, new Object[] {});

	}

	@Override
	public List<?> getMasterCopyOfAgreementwithTheEntity() {
		 return iGenericDao.executeDDLHQL(JavaConstant.QUERY_agreement, new Object[] {});

	}

	@Override
	public List<?> getMasterFoodCategory() {
		 return iGenericDao.executeDDLHQL(JavaConstant.QUERY_food, new Object[] {});
	}

	@Override
	public List<?> getMasterIngredient() {
		 return iGenericDao.executeDDLHQL(JavaConstant.QUERY_ingredient, new Object[] {});
	}

	@Override
	public List<?> getMasterModule() {
		 return iGenericDao.executeDDLHQL(JavaConstant.QUERY_module, new Object[] {});

	}

	@Override
	public List<?> getMasterPaymentMode() {
		return iGenericDao.executeDDLHQL(JavaConstant.QUERY_payment, new Object[] {});
	}

	@Override
	public List<?> getMasterRegulatoryStatus() {
		return iGenericDao.executeDDLHQL(JavaConstant.QUERY_status, new Object[] {});
	}

	@Override
	public List<?> getMasterRole() {
		return iGenericDao.executeDDLHQL(JavaConstant.QUERY_role, new Object[] {});

	}

	@Override
	public List<?> getMasterSafetyInformation() {
		return iGenericDao.executeDDLHQL(JavaConstant.QUERY_safety, new Object[] {});

	}

	@Override
	public List<?> getMasterSourceOfFoodIngradient() {
		return iGenericDao.executeDDLHQL(JavaConstant.QUERY_source, new Object[] {});

	}

	@Override
	public List<?> getMasterStatus() {
		return iGenericDao.executeDDLHQL(JavaConstant.QUERY_STATUS, new Object[] {});

	}

	@Override
	public List<?> getMasterSubModule() {
		return iGenericDao.executeDDLHQL(JavaConstant.QUERY_submodule, new Object[] {});

	}

	@Override
	public List<?> getMasterTypeOfFood() {
		return iGenericDao.executeDDLHQL(JavaConstant.QUERY_type, new Object[] {});

	}

	@Override
	public List<?> getMasterUserVsSubModule() {
		return iGenericDao.executeDDLHQL(JavaConstant.QUERY_AA, new Object[] {});

	}

	
}
