package com.epaas.servicesImpl;


import java.math.BigInteger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.epaas.bean.JavaConstant;
import com.epaas.dao.IGenericDao;
import com.epaas.dto.MasterUserDto;
import com.epaas.entities.MasterUser;
import com.epaas.services.SignupServices;

@Service
@Transactional
public class SignupServicesImpl implements SignupServices {

	static String secretKey = "$$CHALLENGE";

	@Autowired
	IGenericDao igenericDao;
	
//	@Autowired
//	@Qualifier("readWriteJpaDao")
//	IGenericDao readWriteDao;

	@Override
	public String saveUserDetails(MasterUserDto masterUserDto) {
		if (masterUserDto != null) {
			MasterUser masterUser = new MasterUser();
			masterUser.setLoginId(masterUserDto.getLoginId());
			masterUser.setUserPwd(masterUserDto.getUserPwd());
			masterUser.setDevPwd(masterUserDto.getDevPwd());
			masterUser.setCompanyName(masterUserDto.getCompanyName());
			masterUser.setNameOfBusiness(masterUserDto.getNameOfBusiness());
			masterUser.setLicenseNo(masterUserDto.getLicenseNo());
			masterUser.setEmailId(masterUserDto.getEmailId());
			masterUser.setAciveFlag(true);
			masterUser.setEmailIdVerifiedFlag(true);
			masterUser.setMobileNo(masterUserDto.getMobileNo());
			masterUser.setRoleId(1);
			masterUser.setMobileNoVerifiedFlag(true);
			masterUser.setUserImgPath(masterUserDto.getUserImgPath());
			igenericDao.save(masterUser);
		}
		return "Saved";
	}

	@Override
	@Transactional
	public Object getUserDetails(BigInteger userId) {
		return igenericDao.executeDDLHQL(JavaConstant.QUERY_user_details, new Object[] { userId }).get(0);
	}

	@Override
	public boolean updateUserDetails( MasterUserDto masterUserDto) {

		boolean updateFlag = false;

		try {
			igenericDao.executeDMLHQL(JavaConstant.QUERY_user_update, new Object[] { masterUserDto.getUserPwd(), masterUserDto.getDevPwd(),
					masterUserDto.getCompanyName(), masterUserDto.getNameOfBusiness(), 
					masterUserDto.getLicenseNo(), masterUserDto.getEmailId(),masterUserDto.getMobileNo(), masterUserDto.getUserImgPath(), masterUserDto.getUserId()  });
			updateFlag = true;
		} catch (Exception e) {
			e.printStackTrace();
			updateFlag = false;
		}
		return updateFlag;
	}

}
