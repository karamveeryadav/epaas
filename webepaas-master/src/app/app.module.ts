import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ContainersModule } from "./shared/containers";
import { ComponentsModule } from "./shared/components";
import { AppRoutingModule } from "./app-routing.module";
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ApiClientModule } from "./shared/api-client";
import { AppComponent } from './app.component';
import { SimpleNotificationsModule } from "angular2-notifications";
import { HTTPListener, HTTPStatus } from "./shared/api-client/interceptor";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgxSpinnerModule } from "ngx-spinner";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AppConfigService } from './app-config'
import { UtilityModule } from "./shared/utility";
import { RegisterComponent } from './pages/register/register.component';
import { LoginComponent } from './pages/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { IngredientServicesService } from './services/ingredient-services.service';






const RxJS_Services = [HTTPListener, HTTPStatus];

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    LoginComponent,
    DashboardComponent,
    
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ContainersModule,
    ComponentsModule,
    SimpleNotificationsModule.forRoot(),
    ApiClientModule.forRoot(),
    UtilityModule.forRoot(),
    NgxSpinnerModule, 
    FormsModule,
    ReactiveFormsModule,
    NgxPaginationModule
  ],
  exports: [
    ContainersModule,
    ComponentsModule,
    HttpClientModule,
    NgxSpinnerModule,
    BrowserAnimationsModule
    
  ],
  providers: [
    RxJS_Services,
    IngredientServicesService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HTTPListener,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      multi: true,
      deps: [AppConfigService],
      useFactory: (appConfigService: AppConfigService) => () => appConfigService.loadAppConfig()
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
