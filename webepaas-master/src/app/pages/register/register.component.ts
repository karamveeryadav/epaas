import { Component, OnInit } from '@angular/core';

import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import {FormBuilder,FormGroup,FormControl,Validators} from "@angular/forms";
// import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
// import { SessionStorageService } from "src/app/services/session-storage.service";
import { CommonService } from "src/app/services/common.service";
import { ApiserviceService } from "src/app/services/apiservice.service";

// import { APIConstant } from "src/app/constants/apiConstants";
import { regExConstant } from 'src/app/shared/constants/regExConstant';
import { validationMessage } from "src/app/shared/constants/validationMessage";
import { APIConstant } from 'src/app/shared/constants/apiConstant';
import { navigationConstants } from 'src/app/shared/constants/navigationConstant';
// import { navigationConstants } from "src/app/constants/navigationConstant";
// import { customStorage } from "src/app/constants/storageKeys";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  validationMessage=validationMessage;
  registerForm!: FormGroup;
  otpregisterForm!: FormGroup;
  forgetPassword!: FormGroup;
  isRegisterFormSubmitted = false;
  btnSpinner:boolean=false;
  isotpRegisterFormSubmitted= false;
  mobileNumberReadOnly:boolean=false;
otpValue:any='';
modelContent:any[]=[];

  constructor(private router: Router,
    private commonService: CommonService,
    private apiserviceService: ApiserviceService,
    public formBuilder: FormBuilder,
    // public sessionStorageService: SessionStorageService,
    // private modalService: NgbModal,
    private spinner: NgxSpinnerService) { }

    ngOnInit(): void {
      this.spinner.show();
      setTimeout(() => {
        this.spinner.hide();
      }, 1000);
      this.registerForm = this.formBuilder.group({
        name: ["", Validators.required],
        email: ["", [Validators.required,Validators.pattern(regExConstant.email)]],
        mobileno: ["", [Validators.required,Validators.pattern(regExConstant.mobileNumber)]],
        orgname: ["", Validators.required],
        busnature: ["",Validators.required],
        otpVerify:[""],
        captcha: ["", Validators.required],
        captcha1: ["", Validators.required],
        captcha2: ["", Validators.required],
        // password: ["", Validators.required],
        // c_password: ["", Validators.required]
      });
      this.otpregisterForm = this.formBuilder.group({
        code: ["", Validators.required],
  
      });
    }
    get registerFormControl() { return this.registerForm.controls; }
    get otpregisterFormControl() { return this.otpregisterForm.controls; }
    onOtpChange(){
      // this.otpValue = val;
    }
    openRegisterModal(modelContent:any) {
      this.spinner.show('loginModelPopup');
      // const otp_send = {mobile_number:this.registerForm.get('mobileno').value};
      // this.apiserviceService.post(APIConstant.OTP_SEND,otp_send,{}).subscribe(data=>{
      //   this.btnSpinner=false;
      //   this.spinner.hide('loginModelPopup');
      // },error=>{
      //   this.spinner.hide('loginModelPopup');
      //   this.btnSpinner=false;
      // });
      // this.modalService.open(modelContent,{
      //   size:"md",
      //   backdrop : 'static',
      //   windowClass : "modalClass-700",
      //   keyboard : false,
      //   ariaLabelledBy: "modal-basic-title"
      // }).result.then(result => {
      //     // this.closeResult = `Closed with: ${result}`;
      //     // this.resetRegistration();
      //   },reason => {
      //     // this.closeResult = `Dismissed`;
      //     // this.resetRegistration();
      //   }
      // );
    }
     onRegisterFormSubmit(modelContent:any){
      console.warn("biki");
  
      this.isRegisterFormSubmitted=true;this.apiserviceService.post(APIConstant.REGISTER,this.registerForm.value,{}).subscribe(data=>{this.btnSpinner=false;
        this.spinner.hide('registrationPopup');
          setTimeout(() => {
            this.commonService.redirectToPage(navigationConstants.LOGIN);
          }, 2000);
        },error=>{
          this.spinner.hide('registrationPopup');
          this.btnSpinner=false;
        });
     
    //   if (this.registerForm.invalid) {
    //     return;
    //   } else if((this.registerFormControl.captcha1.value+this.registerFormControl.captcha2.value)!=this.registerFormControl.captcha.value){
    //     // this.commonService.notification(this.validationMessage.toasterClass.error, this.validationMessage.login.captchaRequire);
    //   } else {
  
    //     // if(this.registerForm.get('otpVerify').value==0){
    //     //   //this.registerForm.get('otpVerify').setValue(1);
    //     //   this.openRegisterModal(modelContent);
    //     // }else{
          // this.spinner.show('registrationPopup');
          // this.btnSpinner=true;
    //     //   this.apiserviceService.post(APIConstant.REGISTER,this.registerForm.value,{}).subscribe(data=>{
    //     //     this.btnSpinner=false;
    //     //     this.spinner.hide('registrationPopup');
    //     //       setTimeout(() => {
    //     //         this.commonService.redirectToPage(navigationConstants.LOGIN);
    //     //       }, 2000);
    //     //   },error=>{
    //     //     this.spinner.hide('registrationPopup');
    //     //     this.btnSpinner=false;
    //     //   });
    //     // }
  
  
    //   }
    }
    onotpRegisterFormSubmit(){
  
      this.isotpRegisterFormSubmitted=true;

      if (this.otpValue=='undefined' || this.otpValue.length!=6) {
        // this.commonService.notification(this.validationMessage.toasterClass.error, this.validationMessage.login.otpRequire);
        return;
      } else {
  
        //   this.btnSpinner=true;
        //   const otp_verified = {
        //    code: this.otpValue,
        //   //  mobile_number:this.registerForm.get('mobileno').value
        //   }
        //   this.spinner.show('loginModelPopup');
        //   this.apiserviceService.post(APIConstant.OTP_VERIFIED,otp_verified,{}).subscribe(data=>{
        //     this.spinner.hide('loginModelPopup');
        //     this.modalService.dismissAll();
        //     this.registerForm.get('otpVerify').setValue(1);
        //     this.mobileNumberReadOnly=true;
        // this.onRegisterFormSubmit();
        //   },error=>{
        //     this.mobileNumberReadOnly=false;
        //     this.spinner.hide('loginModelPopup');
        //     this.btnSpinner=false;
        //   });
  
  
  
      }
    }

}
