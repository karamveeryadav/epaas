import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from "src/environments/environment";

// function _window():any{
//   return window;
// }

@Injectable({
  providedIn: 'root'
})
export class ApiserviceService {
  [x: string]: any;
  private baseUrl = environment.apiUrl;
  get nativeWindow(): any {
    return;
  }

  constructor(
    private http: HttpClient
  ) { }

  post(url:string,payload:any,params:any){
    return this.http.post<any>(this.baseUrl+url,payload,{params: params}).pipe(
      // retry(1),
      // catchError(this.errorHandl)
    );
  }
//   get(url:string,params:any){
//     // return this.http.get(this.baseUrl+url,{params: params});
//     return this.http.get<any>(this.baseUrl+url,{params: params}).pipe(
//       // retry(1),
//       catchError(this.errorHandl)
//     );
//   }
//   post(url:string,payload:any,params:any){
//     return this.http.post<any>(this.baseUrl+url,payload,{params: params}).pipe(
//       // retry(1),
//       catchError(this.errorHandl)
//     );
//   }
//   postFile(url:string,payload:any,params:any){
//     const options={
//       params: params,
//       // headers:{'Content-Type':'multipart/form-data; charset=utf-8; boundary=' + Math.random().toString().substr(2)}
//     }
//     return this.http.post<any>(this.baseUrl+url,payload,options).pipe(
//       // retry(1),
//       catchError(this.errorHandl)
//     );
//   }
//   delete(url:string,params:any){
//     return this.http.delete<any>(this.baseUrl+url,{params: params}).pipe(
//       // retry(1),
//       catchError(this.errorHandl)
//     );
//   }
//   third_party_post(url:string,payload:any,params:any){
//     return this.http.post<any>(url,payload,{params: params}).pipe(
//       // retry(1),
//       catchError(this.errorHandl)
//     );
//   }
//   third_party_get(url:string,params:any){
//     // return this.http.get(this.baseUrl+url,{params: params});
//     return this.http.get<any>(url,{params: params}).pipe(
//       // retry(1),
//       catchError(this.errorHandl)
//     );
//   }
//   errorHandl(error: { error: { message: string; }; status: any; message: any; }) {
//     let errorMessage = '';
//     if(error.error instanceof ErrorEvent) {
//       // Get client-side error
//       errorMessage = error.error.message;
//     } else {
//       // Get server-side error
//       console.log(error);
//       errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
//     }
//     // console.log('api service errr');
//     // console.log(errorMessage);
//     return throwError(error);  //Rethrow it back to component
//  }
//  getExt(url:string,params:any){
//   // return this.http.get(this.baseUrl+url,{params: params});
//   return this.http.get<any>(url,{params: params}).pipe(
//     // retry(1),
//     catchError(this.errorHandl)
//   );
// }
}
