import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
// import { ToastrService } from "ngx-toastr";

// import { SessionStorageService } from "src/app/services/session-storage.service";
import { ApiserviceService } from "src/app/services/apiservice.service";
// import { SharedService } from "src/app/services/shared.service"
// import { customStorage } from "src/app/constants/storageKeys";
import { validationMessage } from "src/app/shared/constants/validationMessage";
// import { APIConstant } from "src/app/constants/apiConstants";
// import { navigationConstants } from "src/app/constants/navigationConstant";
import { NgxSpinnerService } from "ngx-spinner";
import { customStorage } from '../shared/constants/storagekeys';
import { APIConstant } from '../shared/constants/apiConstant';
// import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  [x: string]: any;
  hexBase = 15;
  validationMessage=validationMessage;


  constructor(
    // private sharedService: SharedService,
    // private toastr: ToastrService,
    // private sessionStorageService: SessionStorageService,
    private router: Router,
    private apiserviceService: ApiserviceService,
    private spinner: NgxSpinnerService,
    // private modalService: NgbModal
  ) { }

  notification(noticationType: string, message: string, title?: string) {
    // this.toastr.clear();
    // this.toastr[noticationType](message, title || noticationType, {
    //   positionClass: "toast-bottom-right",
    //   timeOut: 5000,
    //   maxOpened: 1,
    //   preventDuplicates: true,
    //   autoDismiss: true
    // });
  }
  getToken(){
    // let token=null;
    // let toeknGet=this.sessionStorageService.getData(customStorage.sessionKeys.token)
    // if(toeknGet!=null && toeknGet!=undefined && toeknGet!=''){
    //   token=toeknGet;
    // }
    // return token;
  }
  redirectToPage(url:string) {
    this.router.navigate([url]);
  }

  clearSession(){
    sessionStorage.clear();
  }
  logout(){
    this.clearSession();
    // this.redirectToPage(navigationConstants.LOGIN);
  }
  applicationConfiguration(){
    // load master and configuration data here in session storage
    this.apiserviceService.get(APIConstant.GET_CONFIGURATION,{}).subscribe((res: { data: { dataGet: { log_request_method: any; max_login_attempt: any; pageLength: any; token_validity: any; }; }; })=>{
      this.sessionStorageService.setData(customStorage.sessionKeys.log_request_method,res.data.dataGet.log_request_method);
      this.sessionStorageService.setData(customStorage.sessionKeys.max_login_attempt,res.data.dataGet.max_login_attempt);
      this.sessionStorageService.setData(customStorage.sessionKeys.pageLength,res.data.dataGet.pageLength);
      this.sessionStorageService.setData(customStorage.sessionKeys.token_validity,res.data.dataGet.token_validity);
      //console.log('We are inside pagiconfig');
      this.sharedService.shareConfigStatus(true);
    },(error: any)=>{
      this.notification(this.validationMessage.toasterClass.error, this.validationMessage.common.applicationError);
    });

    this.apiserviceService.get(APIConstant.STATES,{}).subscribe((res: { data: { dataGet: any; }; })=>{
      if(res.data && res.data.dataGet){
        this.sessionStorageService.setData(customStorage.sessionKeys.state,JSON.stringify(res.data.dataGet));
      } else {
        this.notification(this.validationMessage.toasterClass.error, this.validationMessage.common.applicationError);
      }
    },(error: any)=>{
      this.sessionStorageService.setData(customStorage.sessionKeys.state,[]);
    });
  }
  encrypt(value: number) {
    return this.Base64EncodeUrl(btoa(value.toString(this.hexBase)));
  }

  decrypt(value: string) {
    try {
      return parseInt(this.Base64DecodeUrl(atob(value)), this.hexBase);
    } catch {
      this.router.navigate(["/404"]);
    }
    return 0;
  }
  Base64EncodeUrl(str:any) {
    return str
      .replace(/\+/g, "-")
      .replace(/\//g, "_")
      .replace(/\=+$/, "");
  }
  Base64DecodeUrl(str:any) {
    if (str.length % 4 != 0) str += "===".slice(0, 4 - (str.length % 4));
    return str.replace(/-/g, "+").replace(/_/g, "/");
  }
  getPermission(module:any,subodule:any){
    // let permissionData=JSON.parse(this.sessionStorageService.getData(customStorage.sessionKeys.permissions));
    // if(module && !subodule){
    //   return permissionData[module];
    // } else {
    //   return permissionData[module].subModule[subodule];
    // }
  }
  applicationProcessNew(dataObject:any){
    console.log(dataObject);
    let params = {
      'assigned_to_role_id':5,
      'assigned_to_user_id':dataObject.assigned_to_user_id,
      'remarks':dataObject.remarks ,
      'application_id':dataObject.application_id,
      'assigned_by_user_id':dataObject.assigned_by_user_id,
      'assigned_by_role_id':dataObject.assigned_by_role_id,
      'status':1
    };
    this.spinner.show('forwardApplicationLoader');
    return true;
    // this.apiserviceService.post(APIConstant.APPLICATION_PROCESS,params,{}).subscribe((res)=>{
    //   if(res){
    //     return 'Y';
    //   } else {
    //     return 'N';
    //   }
    // },error=>{
    //   return 'N';
    // });
  }
}
