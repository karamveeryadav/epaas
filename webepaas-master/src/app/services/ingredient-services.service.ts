import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class IngredientServicesService {

  constructor(private http:HttpClient) { }
  getIngredients(){
    //  let ingredientsApiURL = "https://jsonplaceholder.typicode.com/users";

    let ingredientsApiURL = "https://infolnet.fssai.gov.in/infolnetv2/common/foodcategory/3";
    return this.http.get(ingredientsApiURL);
  }
  getAdditives(){
    // let additivesAPIURL = "https://jsonplaceholder.typicode.com/todos";
    let additivesAPIURL = "https://infolnet.fssai.gov.in/infolnetv2/common/getAdditiveList";
    return this.http.get(additivesAPIURL);
  }
}
