import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators'
import { AppConfigService } from '../../app-config'

@Injectable()
export class AnnualReturnService {
  constructor(
    private http: HttpClient,
    private appConfigService: AppConfigService
  ) { }

  getCaptcha() {
    return this.http
      .get(this.appConfigService.get("api").baseUrl + "/annual_return/tokenfreearcontroller/captcha", {
        responseType: "blob", observe: "response"
      })
      .pipe(map((response: HttpResponse<any>) => {
        return response;
      }));
  }

  getLicenseDetailsForReturn(licenseNo: string, expiryDate: string): Observable<any> {
    return this.http.get(this.appConfigService.get("api").baseUrl + "/annual_return/annualreturn/getlicensedetailsforannualreturn/" + licenseNo + "/" + expiryDate, { observe: "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postUserAuthToken(postData: any): Observable<any> {
    return this.http.post(this.appConfigService.get("api").baseUrl + "/auth", JSON.stringify(postData), { observe: "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postOTPWithoutValidation(postData: any): Observable<any> {
    return this.http.post(this.appConfigService.get("api").baseUrl + "/annual_return/tokenfreearcontroller/sendotpwithoutvalidationforreturn", JSON.stringify(postData), { observe: "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getAnnualReturnDetail(licenseNo: any): Observable<any[]> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/listtofillforannualreturn/' + licenseNo, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getAnnualReturnPDF(annualReturnRefId: any) {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/downloadpdf/annualreturn/' + annualReturnRefId, { responseType: 'blob', observe: 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response;
      }));
  }

  getAnnualReturnLicDetails(postJson: any): Observable<any[]> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/listofkobforannualreturn/', JSON.stringify(postJson), { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  fileAnnualReturnToGenerateRefId(postJson: any): Observable<any> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/posttogeneraterefid', JSON.stringify(postJson), { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }


  getAnnualReturnProduct(annualReturnRefId: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getmanufacturesdetails/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }


  getCategory(): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getproduct', { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getSubFoodCategory(postData: any, flag: boolean): Observable<any[]> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/subfoodcategory/' + flag, JSON.stringify(postData), { "observe": "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getProduct(postData: any): Observable<any[]> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/foodproducts/', JSON.stringify(postData), { "observe": "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postProductDetails(postJson: any): Observable<any> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/savemanufacturesdetails', JSON.stringify(postJson), { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  deleteProduct(productId: any): Observable<any> {
    return this.http.delete(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deletemanufacturesdetails/' + productId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getSource(annualReturnRefId: number, sourceId: number): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getFboAnnualReturnMajorSourceAndFbosData/' + annualReturnRefId + '/' + sourceId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  deleteSource(id: number): Observable<any> {
    return this.http.delete(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deleteFboAnnualReturnMajorSourceAndFbosData/' + id, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postSource(postJson: any): Observable<any> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/saveFboAnnualReturnMajorSourceData', JSON.stringify(postJson), { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postAnnualReturnDoc(file: File, data: string): any {

    let formdata: FormData = new FormData();
    formdata.append('jsonInput', data);
    formdata.append('documentfile', file);
    const req = new HttpRequest('POST', this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/uploadannualreturndocument', formdata, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }


  deleteAnnualReturnDoc(postData: any): Observable<any[]> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deleteannualreturndocument', postData, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));

  }

  getViewUploadDoucments(path: any): Observable<HttpResponse<Blob>> {
    return this.http.get(this.appConfigService.get('api').baseUrl + path, { responseType: 'blob', 'observe': 'response' })
  }

  getFostacDetails(annualReturnRefId: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getfboannualreturnfostacdetails/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postFostacData(postJsonFostac: any): Observable<any> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/savefboannualreturnfostacdetails', postJsonFostac, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postProductBriefingDoc(file: File, data: string): any {

    let formdata: FormData = new FormData();
    formdata.append('jsonInput', data);
    formdata.append('documentfile', file);
    const req = new HttpRequest('POST', this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/uploadmanufacturedocument', formdata, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }

  deleteProductBriefingDoc(postData: any): Observable<any[]> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deletemanufacturedocument', postData, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  addProductBriefingDetails(postJson: any): Observable<any> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/savemanufacturesaddproductdetails', postJson, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getSubmittedAnnualReturnForm(annualReturnRefId: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getannualreturnformdetailsbyrefid/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getOilName(): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/masterProductForOilConsumed/', { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  saveAnnualReturmOilConsumed(postJson: any) {
    return this.http.post(this.appConfigService.get('api').baseUrl + "/annual_return/annualreturn/saveAnnualReturnOilConsumed", postJson, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getConsumedOilDetails(annualReturnRefId: any) {
    return this.http.get(this.appConfigService.get('api').baseUrl + "/annual_return/annualreturn/viewAnnualReturnOilConsumedDetails/" + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  deleteConsumedOil(oilConsumedId: number, annualReturnRefId: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deleteAnnualReturnOilConsumed/' + oilConsumedId + '/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getAggregatorDetail(annualReturnRefId: any) {
    return this.http.get(this.appConfigService.get('api').baseUrl + "/annual_return/annualreturn/viewUCOAggregatorDetails/" + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }
  getAggregatorList(): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/masterAggregatorNameList/', { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }
  postAggregator(postAggregator: any) {
    return this.http.post(this.appConfigService.get('api').baseUrl + "/annual_return/annualreturn/saveUCOAggregatorDetails", postAggregator, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  deleteAggregator(ucoId: number, annualReturnRefId: number): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deleteUCOAggregator/' + ucoId + '/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  confirmDeleteLicense(annualReturnRefId: number): Observable<any> {
    return this.http.delete(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deletefboannualreturndetailsdata/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getSlaughteringDetails(annualReturnRefId: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getFboAnnualReturnSlaughteringData/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postSlaughteringOrImporterData(postJsonSlaughtering: any): Observable<any> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/saveFboAnnualReturnSlaughteringData', postJsonSlaughtering, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  deleteSlaughteringOrImporterData(productId: any, annualReturnRefId: any): Observable<any> {
    return this.http.delete(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deleteFboAnnualReturnSlaughteringData/' + productId + '/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }


  getMajorSourceOfAnimals(annualReturnRefId: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getMajorSourceOfAnimals/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postSourceOfAnimalsData(postMajorSourceDataJson: any): Observable<any> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/savemajorsourceffanimals', postMajorSourceDataJson, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  deleteSourceOfAnimals(sourceId: any): Observable<any> {
    return this.http.delete(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deleteMajorSourceOfAnimals/' + sourceId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getProcessingDetails(annualReturnRefId: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getFboAnnualReturnProcessingData/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postProcessingData(file: File, data: string): any {

    let formdata: FormData = new FormData();
    formdata.append('jsonInput', data);
    formdata.append('documentfile', file);
    const req = new HttpRequest('POST', this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/savefboannualreturnprocessingdata', formdata, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);

  }

  postProcessingDataWithoutImage(postProcessingJson: any): Observable<any> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/savefboannualreturntableprocessingdata', postProcessingJson, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postReportDoc(file: File, data: string): any {

    let formdata: FormData = new FormData();
    formdata.append('jsonInput', data);
    formdata.append('documentfile', file);
    const req = new HttpRequest('POST', this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/saveprocessingreportdocdata', formdata, {
      reportProgress: true,
      responseType: 'text'
    });
    return this.http.request(req);
  }

  getImageForProcessingProductModel(annualReturnRefId: any, id: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getprocessingproductimages/' + annualReturnRefId + '/' + id, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  deleteTestReport(postData: any): Observable<any[]> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deletetestreportdocument', postData, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }


  getImportersDetails(annualReturnRefId: any, annualKobId: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getAllProductDetails/' + annualReturnRefId + '/' + annualKobId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getImportersWarehousesDetails(annualReturnRefId: any): Observable<any> {
    return this.http.get(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/getimporterswarehouses/' + annualReturnRefId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getStateMaster(): Observable<any[]> {
    return this.http.get<any[]>("/assets/stateMaster.json", { "observe": "response" })
      //return this.http.get<any[]>(this.appConfigService.get('api').baseUrl + "/common/address/getstatelist", { "observe": "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getDistrictMaster(postData: any): Observable<any[]> {
    return this.http.post<any[]>(this.appConfigService.get('api').baseUrl + "/annual_return/annualreturn/getdistictlist", JSON.stringify(postData), { "observe": "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postImporterWarehouseData(postJson: any): Observable<any> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/saveimporterswarehouses', JSON.stringify(postJson), { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  deleteWarehousesImporters(warehousesId: number): Observable<any> {
    return this.http.delete(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/deleteimporterswarehouses/' + warehousesId, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getExporterFormDataForExporter(annualReturnRefId: any): Observable<any[]> {
    return this.http.get(this.appConfigService.get('api').baseUrl + "/annual_return/annualreturn/getFboAnnualReturnExportingDetails/" + annualReturnRefId, { "observe": "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  postAnnualExportFormForExporter(postData: any): Observable<any[]> {
    return this.http.post(this.appConfigService.get('api').baseUrl + '/annual_return/annualreturn/saveFboAnnualReturnExportingDetails', postData, { 'observe': 'response' })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  getCountryMaster(): Observable<any[]> {
    return this.http.get<any[]>(this.appConfigService.get('api').baseUrl + "/annual_return/annualreturn/mastercountry", { "observe": "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }

  deleteExportFormDetailForExporter(exportingDetailsId: number): Observable<any[]> {
    return this.http.delete(this.appConfigService.get('api').baseUrl + "/annual_return/annualreturn/deleteFboAnnualReturnExportingDetails/" + exportingDetailsId, { "observe": "response" })
      .pipe(map((response: HttpResponse<any>) => {
        return response.body;
      }));
  }
}