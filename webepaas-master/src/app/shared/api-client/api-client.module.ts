import {
  NgModule,
  ModuleWithProviders
} from "@angular/core";

import { AnnualReturnService } from './annual-return.service';

@NgModule()
export class ApiClientModule {
  static forRoot(): ModuleWithProviders<ApiClientModule> {
    return {
      ngModule: ApiClientModule,
      providers: [
        AnnualReturnService
      ]
    };
  }


}
