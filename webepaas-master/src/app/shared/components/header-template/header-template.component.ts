import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-header-template',
  templateUrl: './header-template.component.html',
  styleUrls: ['./header-template.component.css'],
})
export class HeaderTemplateComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  fboSignIn() {
    window.location.href = getHostName() + "/public";
  }

  officerSignIn() {
    window.location.href = getHostName() + "/officer";
  }
}

export function getHostName(): string {
  let currentUrl = window.location.protocol + "//" + window.location.hostname;
  return currentUrl;
}