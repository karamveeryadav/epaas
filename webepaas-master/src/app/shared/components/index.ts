import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

import { FooterTemplateComponent } from "./footer-template/footer-template.component";
import { HeaderTemplateComponent } from "./header-template/header-template.component";


export const COMPONENTS = [
  FooterTemplateComponent,
  HeaderTemplateComponent
];

@NgModule({
  imports: [
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS
})
export class ComponentsModule { }
