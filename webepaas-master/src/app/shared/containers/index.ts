import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../components';
import { MainLayoutContainer } from './layout/main-layout.container';
import { RouterModule } from '@angular/router';
import { LayoutSandbox } from './layout/layout.sandbox';
export const CONTAINERS = [
  MainLayoutContainer
];

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    RouterModule,

  ],
  declarations: [MainLayoutContainer],
  exports: [MainLayoutContainer],
  providers: [LayoutSandbox],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ContainersModule { }