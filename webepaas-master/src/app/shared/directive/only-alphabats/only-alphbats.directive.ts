import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[AllowOnlyAlphabats]'
})
export class OnlyAlphbatsDirective {

  @Input() AllowOnlyAlphabats: boolean = false;

  constructor() { }

  @HostListener('keydown', ['$event']) onKeyDown(event: any) {
    let e = <KeyboardEvent>event;
    //let input = event.target as HTMLInputElement;
    if (this.AllowOnlyAlphabats) {
      if ([32, 8, 9, 27].indexOf(e.keyCode) !== -1 ||
        // Allow: Ctrl+A
        (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
        // Allow: Ctrl+C
        (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
        // Allow: Ctrl+V
        (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
        // Allow: Ctrl+X
        (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
        // Allow: home, end, left, right
        (e.keyCode >= 35 && e.keyCode <= 39)) {
        // let it happen, don't do anything
        return;
      }
      //&& e.shiftKey
      // Ensure that it is a number and stop the keypress
      if ((e.keyCode < 65 || e.keyCode > 90)) {
        e.preventDefault();
      }
    }
  }
}
