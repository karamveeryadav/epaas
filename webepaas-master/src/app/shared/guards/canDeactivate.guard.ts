import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

export interface CanComponentDeactivate {
  canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable()
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {

  /*  constructor(private permissions: Permissions, private currentUser: UserToken) {} */
  canDeactivate(
    component: CanComponentDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    return false;
    /*  return this.permissions.canDeactivate(this.currentUser) */
  }

  /* class UserToken {}
  class Permissions {
    router: any;
    canDeactivate(user: UserToken): boolean {
      user = sessionStorage.getItem('accessToken')
      if (user) { return true; }
  
      // Navigate to the login page with extras
      this.router.navigate(['/']);
      return false;
    } */
}

