import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'mobileNoMasking',
    pure: false
})

export class MobileNoMaskingPipe implements PipeTransform {

    constructor() { }

    transform(mobileNo: string): string {
        let arrMobile = mobileNo.split('');
        arrMobile.forEach((item, index) => {
            if (index == 0 || index == 1 || index == arrMobile.length - 1 || index == arrMobile.length - 2) {
            }
            else {
                arrMobile[index] = 'X';
            }
        });
        return arrMobile.join('');
    }
}