import { PipeTransform, Pipe } from '@angular/core'

@Pipe({ name: 'separatedDelimiter' })
export class SeparatedDelimiterPipe implements PipeTransform {
  transform(records: Array<any>, keyName: string, delimiter?: string): any {
    let newRecord: string = '';
    delimiter = delimiter == null || delimiter == '' ? ', ' : delimiter;
    newRecord = records.map(x => x[keyName]).join(delimiter);
    return newRecord;
  }
}