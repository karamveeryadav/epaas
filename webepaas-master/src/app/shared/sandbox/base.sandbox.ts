export abstract class Sandbox {
  constructor() { }
  public getUserDetail(): any {
    let userDetails = JSON.parse(sessionStorage.getItem('userSession') as string);
    return userDetails;
  }
}