import {
  NgModule,
  ModuleWithProviders
} from "@angular/core";
import { UtilService } from './utility.service';
import { WindowRef } from './WindowRef.service';
@NgModule()
export class UtilityModule {
  static forRoot(): ModuleWithProviders<UtilityModule> {
    return {
      ngModule: UtilityModule,
      providers: [
        UtilService,
        WindowRef
      ]
    };
  }
}