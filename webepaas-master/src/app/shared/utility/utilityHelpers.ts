/* import { Observable } from 'rxjs';
import { Store } from '@ngrx/store'; */

let typeCache: { [label: string]: boolean } = {};

type Predicate = (oldValues: Array<any>, newValues: Array<any>) => boolean;

/**
 * This function coerces a string into a string literal type.
 * Using tagged union types in TypeScript 2.0, this enables
 * powerful typechecking of our reducers.
 * 
 * Since every action label passes through this function it
 * is a good place to ensure all of our action labels are unique.
 *
 * @param label
 */
export function type<T>(label: T | ''): T {
  if (typeCache[<string>label]) {
    throw new Error(`Action type "${label}" is not unqiue"`);
  }

  typeCache[<string>label] = true;

  return <T>label;
}

/**
 * Runs through every condition, compares new and old values and returns true/false depends on condition state.
 * This is used to distinct if two observable values have changed.
 *
 * @param oldValues
 * @param newValues
 * @param conditions
 */
export function distinctChanges(oldValues: Array<any>, newValues: Array<any>, conditions: Predicate[]): boolean {
  if (conditions.every(cond => cond(oldValues, newValues))) return false;
  return true;
}

/**
 * Returns true if the given value is type of Object
 *
 * @param val
 */
export function isObject(val: any) {
  if (val === null) return false;

  return ((typeof val === 'function') || (typeof val === 'object'));
}

/**
 * Capitalizes the first character in given string
 *
 * @param s
 */
export function capitalize(s: string) {
  if (!s || typeof s !== 'string') return s;
  return s && s[0].toUpperCase() + s.slice(1);
}

export function strUpperCase(s: string) {
  if (!s || typeof s !== 'string') return s;
  return s && s.toUpperCase();
}

/**
 * Uncapitalizes the first character in given string
 *
 * @param s
 */
export function uncapitalize(s: string) {
  if (!s || typeof s !== 'string') return s;
  return s && s[0].toLowerCase() + s.slice(1);
}

export function strLowerCase(s: string) {
  if (!s || typeof s !== 'string') return s;
  return s && s[0].toLowerCase()
}

/**
 * Returns formated date based on given culture
 *
 * @param dateString
 * @param culture
 */
export function localeDateString(dateString: string, culture: string = 'en-EN'): string {
  let date = new Date(dateString);
  return date.toLocaleDateString(culture);
}

export function setDateReverseFormat(dateValue: any) {
  
  let year = null;
  let month = null;
  let date = null;
  let dateCont: string[] = [];
  let dateReverseFormat = null;

  if (typeof dateValue === 'string') { // if date is already string then return as it is format.
    dateCont = dateValue.split("-");
    year = dateCont[2].split(",")[0];
    month = dateCont[1].length == 1 ? '0' + dateCont[1] : dateCont[1];
    date = dateCont[0].length == 1 ? '0' + dateCont[0] : dateCont[0];
  }
  else {
    year = dateValue.getFullYear();
    month = (dateValue.getMonth() + 1).toString().length == 1 ? '0' + (dateValue.getMonth() + 1).toString() : dateValue.getMonth() + 1;
    date = dateValue.getDate().toString().length == 1 ? '0' + dateValue.getDate().toString() : dateValue.getDate();//dateValue.getDate();
  }

  dateReverseFormat = (year + '-' + month + '-' + date);
  return dateReverseFormat.toString();
}

export function setDateReverseotherFormat(dateValue: Date) {
  let dateCont: any[] = dateValue.toLocaleString().split("-");
  let dateReverseFormatedit = (dateCont[0] + '/' + dateCont[1] + '/' + dateCont[2])
  return dateReverseFormatedit;
}

export function getHostName(): string {
  let currentUrl = window.location.protocol + "//" + window.location.hostname;
  return currentUrl;
}